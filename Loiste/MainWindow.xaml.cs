﻿using Loiste.Controls;
using Loiste.src;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using Loiste.Pages;
using System.Windows.Media.Animation;
using System.Windows.Media;

namespace Loiste
{
	public partial class MainWindow : Window, IDisposable
	{
		private App app;
		private Mediakeys mediakeys;

		public MainWindow()
		{
			InitializeComponent();

			app = App.Current as App;

			PodcastsButton.IsChecked = true;

			SourceInitialized += new EventHandler(OnSourceInit);
			App.AudioHandler.EpisodeChanged += new EventHandler(OnEpisodeChanged);
			
			if (App.Settings.m_LoadSessionInfo)
			{
				SetLastSessionInfo();
			}

			Console.WriteLine("width :" + ActualWidth);

			if (!Properties.Settings.Default.FirstTime)
			{
				this.Top = Properties.Settings.Default.Top;
				this.Left = Properties.Settings.Default.Left;
				this.Height = Properties.Settings.Default.Height;
				this.Width = Properties.Settings.Default.Width;
				// Very quick and dirty - but it does the job
				if (Properties.Settings.Default.Maximized)
				{
					WindowState = WindowState.Maximized;
				}
			}
			else
			{
				

				
			}

			this.Height = 720;
			this.Width = 1216;


			Properties.Settings.Default.FirstTime = false;
		}

		~MainWindow()
		{
			Dispose (false);
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		protected virtual void Dispose(bool disposing)
		{
			if (disposing)
			{
				mediakeys.Dispose();
			}
		}


		private void OnSourceInit(object sender, EventArgs e)
		{
			mediakeys = new Mediakeys(this);
			mediakeys.PlayPausePressed += new EventHandler((o, args) =>
			{
				if (App.Settings.m_EnableMediakeys)
				{
					App.AudioHandler.PlayPause();
				}
			});
		}

		private void OnEpisodeChanged(object sender, EventArgs e)
		{
			if (App.AudioHandler.CurrentEpisode != null)
			{
				SetTitle("Loiste - " + App.AudioHandler.CurrentEpisode.Title);
			}
			else
			{
				SetTitle("Loiste");
			}
		}


		private void Podcasts_Checked(object sender, RoutedEventArgs e)
		{
			MainFrame.Navigate(App.PodcastsPage);
		}

		private void Queue_Checked(object sender, RoutedEventArgs e)
		{
			//settingspage settingspage = new settingspage();
			//mainframe.navigate(settingspage);

		}

		private void Settings_Checked(object sender, RoutedEventArgs e)
		{
			MainFrame.Navigate(App.SettingsPage);
		}

		private void Window_Closing(object sender, CancelEventArgs e)
		{
			if (WindowState == WindowState.Maximized)
			{
				// Use the RestoreBounds as the current values will be 0, 0 and the size of the screen
				Properties.Settings.Default.Top = RestoreBounds.Top;
				Properties.Settings.Default.Left = RestoreBounds.Left;
				Properties.Settings.Default.Height = RestoreBounds.Height;
				Properties.Settings.Default.Width = RestoreBounds.Width;
				Properties.Settings.Default.Maximized = true;
			}
			else
			{
				Properties.Settings.Default.Top = this.Top;
				Properties.Settings.Default.Left = this.Left;
				Properties.Settings.Default.Height = this.Height;
				Properties.Settings.Default.Width = this.Width;
				Properties.Settings.Default.Maximized = false;
			}

			Properties.Settings.Default.Save();

			mediakeys.Dispose();


			Debug.WriteLine("shutting down loiste. ");
		}


		private void SetTitle(string newTitle)
		{
			Action setTitle = () =>
			{
				Title = newTitle;
			};

			if (CheckAccess())
			{
				setTitle();
			}
			else
			{
				Dispatcher.BeginInvoke(setTitle);
			}
		}

		private void SetLastSessionInfo()
		{
			var lastSessionInfo = app.LoadSessionInfo();

			if (lastSessionInfo == null) return;

			if (lastSessionInfo.episodeGuid == Guid.Empty || lastSessionInfo.podcastGuid == Guid.Empty) return;

			var podcast = App.PodcastHandler.GetPodcastByGuid(lastSessionInfo.podcastGuid);
			if (podcast == null)
			{
				Console.WriteLine("No podcast found with guid: " + lastSessionInfo.podcastGuid);
				return;
			}

			var episode = podcast.GetEpisodeByGuid(lastSessionInfo.episodeGuid);
			if (episode == null)
			{
				Console.WriteLine("No episode found with guid: " + lastSessionInfo.episodeGuid);
				return;
			}

			AudioContoller.PrePlayInfo = new PrePlayInfo 
			{
				podcast = podcast,
				episode = episode,
				duration = lastSessionInfo.duration,
				position = lastSessionInfo.position
			};

			if (App.Settings.m_SessionVolume)
			{
				App.AudioHandler.SetVolume(lastSessionInfo.volume);
			}
			else
			{
				App.AudioHandler.SetVolume(App.Settings.m_DefaultVolume);
			}

			Task.Run(() => App.AudioHandler.Play(podcast, episode, false, lastSessionInfo.position));
		}

		private void CommandBinding_Executed(object sender, System.Windows.Input.ExecutedRoutedEventArgs e)
		{
			Window debugwindow = new Window();
			DebugPage debugPage = new DebugPage();
			debugwindow.Content = debugPage;
			debugwindow.Title = "Debug window";
			debugwindow.Width = 800;
			debugwindow.Height = 400;
			debugwindow.Show();
		}

		private void PlayPauseCommand_Executed(object sender, System.Windows.Input.ExecutedRoutedEventArgs e)
		{
			int i = 0;
		}

		

		private async void RefreshButton_Click(object sender, RoutedEventArgs e)
		{
			try
			{


				var da = new DoubleAnimation(360, 0, new Duration(TimeSpan.FromSeconds(1)));
				var rt = new RotateTransform();
				RefreshButton.RenderTransform = rt;
				RefreshButton.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
				da.RepeatBehavior = RepeatBehavior.Forever;
				rt.BeginAnimation(RotateTransform.AngleProperty, da);

				await App.PodcastHandler.RefreshPodcastsAsync().ConfigureAwait(false);

				Action setTitle = () =>
				{
					rt.BeginAnimation(RotateTransform.AngleProperty, null);
                };

				if (CheckAccess())
				{
					setTitle();
				}
				else
				{
					Dispatcher.BeginInvoke(setTitle);
				}

			}
			catch (Exception ex)
			{
				int i = 0;
			}
		}
	}
}
