﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loiste.src
{
	public class AudioState
	{
		public enum SourceState
		{
			Downloaded, //Audio file exists.
			Downloading, //Currently downloadig, audio will be saved into file.
			DownloadPaused,
			Streaming, //Currently downloading, but audio won't be saved to disk.
			NotFound
		}

		public enum PlaybackState
		{
			Stopped,
			Playing,
			Buffering,
			Paused,
			Ended
		}

		public enum ListeningState
		{
			Finished,
			Unfinished
		}

		public enum SourceType
		{
			Stream,
			File
		}

		public SourceState m_SourceState;
		public PlaybackState m_PlaybackState;
		public SourceType m_SourceType;
		public ListeningState m_ListeningState;

		//public TimeSpan m_PlaybackTime;
	}
}
