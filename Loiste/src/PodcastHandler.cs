﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;

namespace Loiste.src
{
	public class PodcastHandler
	{
		private const string podcastsFile = "podcasts.json";

		public event NotifyCollectionChangedEventHandler PodcastListChanged
		{
			add { m_Podcasts.CollectionChanged += value; }
			remove { m_Podcasts.CollectionChanged -= value; }
		}

		public ObservableCollection<Podcast> m_Podcasts 
		{
			get;
			private set;
		}

		public PodcastHandler()
		{
			m_Podcasts = new ObservableCollection<Podcast>();

			LoadPodcasts();
			LoadEpisodes();
		}

		public Guid[] GetPodcastsGuids()
		{
			var guidArray = new Guid[m_Podcasts.Count()];

			for (int i = 0; i < m_Podcasts.Count(); ++i)
			{
				guidArray[i] = m_Podcasts[i].m_Guid;
			}

			return guidArray;
		}

		public Podcast GetPodcastByGuid(Guid podcastGuid)
		{
			return m_Podcasts.Where(x => x.m_Guid == podcastGuid).First();
		}


		public async Task RefreshPodcastsAsync()
		{
			Debug.WriteLine("id" + Thread.CurrentThread.ManagedThreadId);

            try
			{
				var tasks = new Task[m_Podcasts.Count()];

				for (int i = 0; i < m_Podcasts.Count(); ++i)
					tasks[i] = m_Podcasts[i].RefreshPodcast();

				await Task.WhenAll(tasks).ConfigureAwait(false);
			}
			catch (Exception ex)
			{
				App.NotificationCenter.PushNotification(ex.Message);
			}

			Debug.WriteLine("All podcasts refreshed.");

			SavePodcastsToFile();

			Debug.WriteLine("All podcasts saved to file.");
		}

		public async Task AddPodcastAsync(string rssUrl)
		{
			var podcast = new Podcast();
			podcast.m_DefaultPodcastInfo.FeedUrl = rssUrl;

			await podcast.RefreshPodcast().ConfigureAwait(false);

			m_Podcasts.Add(podcast);

			SavePodcastsToFile();
		}

		public void RemovePodcast(Guid podcastGuid)
		{
			Podcast podcast = null;
			for (int i = m_Podcasts.Count() - 1; i >= 0; i--)
			{
				if (m_Podcasts[i].m_Guid == podcastGuid)
				{
					podcast = m_Podcasts[i];
				}
			}

			m_Podcasts.Remove(podcast);

			SavePodcastsToFile();

			var jsonPodcastFilePath = "podcasts/" + Utils.GetPodcastFilename(podcast) + ".json";
			var podcastImageFilePath = "podcasts/" + podcast.GetPodcastInfo().CoverFilePath;

			if (File.Exists(jsonPodcastFilePath))
			{
				Utils.SafeFileWrite(new Uri(jsonPodcastFilePath, UriKind.Relative), () =>
				{
					File.Delete(jsonPodcastFilePath);
				});
			}

			if (File.Exists(podcastImageFilePath))
			{
				Utils.SafeFileWrite(new Uri(podcastImageFilePath, UriKind.Relative), () =>
				{
					File.Delete(podcastImageFilePath);
				});
			}
		}


		private void LoadPodcasts()
		{
			string podcastJsonFile = "";

			Utils.SafeFileRead(new Uri(podcastsFile, UriKind.Relative), () =>
			{
				podcastJsonFile = System.IO.File.ReadAllText(podcastsFile);
			});

			m_Podcasts = JsonConvert.DeserializeObject<ObservableCollection<Podcast>>(podcastJsonFile);
		}

		private void LoadEpisodes()
		{
			if (!Directory.Exists("podcasts")) return;

			//load episodes
			for (int i = 0; i < m_Podcasts.Count(); ++i)
			{
				var filename = Utils.GetPodcastFilename(m_Podcasts[i]) + ".json";
				
				if (!File.Exists("podcasts/" + filename)) continue;

				string episodesJsonFile = "";

				Utils.SafeFileRead(new Uri("podcasts/" + filename, UriKind.Relative), () =>
				{
					episodesJsonFile = System.IO.File.ReadAllText("podcasts/" + filename);
				});
				
				m_Podcasts[i].m_Episodes = JsonConvert.DeserializeObject<List<Episode>>(episodesJsonFile);
			}
		}

		//TODO: async and save if program closes
		public void SavePodcastsToFile()
		{
			if (!Directory.Exists("podcasts"))
				Directory.CreateDirectory("podcasts");

			//Save episodes
			foreach (Podcast podcast in m_Podcasts)
			{
				string serializedEpisodes = JsonConvert.SerializeObject(podcast.m_Episodes, Newtonsoft.Json.Formatting.Indented);
				var filename = Utils.GetPodcastFilename(podcast) + ".json";

				Utils.SafeFileWrite(new Uri("podcasts /" + filename, UriKind.Relative), () =>
				{
					File.WriteAllText("podcasts/" + filename, serializedEpisodes);
				});
				
			}

			//Save podcasts to one file
			string serializedPodcasts = JsonConvert.SerializeObject(m_Podcasts, Newtonsoft.Json.Formatting.Indented);

			Utils.SafeFileWrite(new Uri(podcastsFile, UriKind.Relative), () =>
			{
				File.WriteAllText(podcastsFile, serializedPodcasts);
			});
		}
	}
}
