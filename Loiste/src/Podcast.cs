﻿using Loiste.src;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using System.ComponentModel;
using System.IO;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace Loiste
{
	public enum RefreshStatus
	{
		NOURL = 0,
		NETWORKERROR,
		RSSPARSEERROR,
		INVALIDRSS,
		UNKNOWNERROR
	}

	[Serializable()]
	public class RefreshFailedException : Exception
	{
		public RefreshStatus m_Status	
		{
			get;
			set;
		}

		public RefreshFailedException(RefreshStatus status = RefreshStatus.UNKNOWNERROR)
			: base()
		{
			m_Status = status;
		}
    
		public RefreshFailedException(string message, RefreshStatus status = RefreshStatus.UNKNOWNERROR)
			: base(message)
		{
			m_Status = status;
		}

		public RefreshFailedException(string message, Exception innerException, RefreshStatus status = RefreshStatus.UNKNOWNERROR)
			: base(message, innerException)
		{
			m_Status = status;
		}

		protected RefreshFailedException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
			m_Status = (RefreshStatus)info.GetInt32("m_Status");
		}
	}

	[Flags]
	public enum RefreshParts
	{
		Title = 0,
		FeedUrl,
		Description,
		CoverUrl,
		All = ~0
	}

	[JsonObject(MemberSerialization.OptIn)]
	public class PodcastInfo : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		protected bool SetField<T>(ref T field, T value, [CallerMemberName] string propertyName = null)
		{
			if (EqualityComparer<T>.Default.Equals(field, value)) return false;
			field = value;
			OnPropertyChanged(propertyName);
			return true;
		}

		private string m_Title_;
		[JsonProperty]
		public string Title
		{
			get { return m_Title_; }
			set { SetField(ref m_Title_, value); }
		}

		private string m_FeedUrl_;
		[JsonProperty]
		public string FeedUrl
		{
			get { return m_FeedUrl_; }
			set { SetField(ref m_FeedUrl_, value); }
		}

		private string m_Description_;
		[JsonProperty]
		public string Description
		{
			get { return m_Description_; }
			set { SetField(ref m_Description_, value); }
		}

		private string m_CoverUrl_;
		[JsonProperty]
		public string CoverUrl
		{
			get { return m_CoverUrl_; }
			set { SetField(ref m_CoverUrl_, value); }
		}

		private string m_CoverFilePath_;
		[JsonProperty]
		public string CoverFilePath
		{
			get { return m_CoverFilePath_; }
			set { SetField(ref m_CoverFilePath_, value); }
		}

		public int RefreshNumber = 0;
	}

	[JsonObject(MemberSerialization.OptIn)]
	public class Podcast
	{
		[JsonProperty]
		public PodcastInfo m_DefaultPodcastInfo { get; private set; }

		[JsonProperty]
		public PodcastInfo m_CustomPodcastInfo;

		[JsonProperty]
		public Guid m_Guid;

		public bool m_Refreshed
		{
			get;
			private set;
		}

		private bool _m_HaveCustomInfo;
		[JsonProperty]
		public bool m_HaveCustominfo
		{
			get
			{
				return _m_HaveCustomInfo;
			}
			private set
			{
				_m_HaveCustomInfo = value;
			}
		}

		public List<Episode> m_Episodes
		{
			get;
			set;
		}


		private PodcastInfo m_RefreshPodcastInfo;

		private object m_RefreshLock = new object();
		private object m_EpisodeLock = new object();

		public event EventHandler PodcastRefreshed;

		public Podcast()
		{
			m_Guid = Guid.NewGuid();
			m_Episodes = new List<Episode>();
			m_DefaultPodcastInfo = new PodcastInfo();
			m_CustomPodcastInfo = new PodcastInfo();
			m_RefreshPodcastInfo = new PodcastInfo();

			m_CustomPodcastInfo.PropertyChanged += new PropertyChangedEventHandler(OnCustomInfoChanged);

			m_Refreshed = false;
		}

		private void OnCustomInfoChanged(object sender, PropertyChangedEventArgs e)
		{
			var info = sender as PodcastInfo;

			var result = info.Title + info.FeedUrl + info.CoverUrl + info.CoverFilePath;
			if (result != "")
			{
				m_HaveCustominfo = true;
			} else
			{
				m_HaveCustominfo = false;
			}

			PodcastRefreshed?.Invoke(this, EventArgs.Empty);
		}

		public Episode GetEpisodeByGuid(Guid episodeGuid)
		{
			return m_Episodes.Where(x => x.m_Guid == episodeGuid).FirstOrDefault();
		}

		public async Task RefreshPodcast(RefreshParts refreshParts = RefreshParts.All)
		{
			lock (m_RefreshLock)
			{
				m_RefreshPodcastInfo.RefreshNumber++;
			}

			XDocument xmlDoc = await GetRssXdoc().ConfigureAwait(false);

			GetPodcastInfo(xmlDoc);

			Task coverResult = DownloadCoverAsync();

			GetEpisodesInfo(xmlDoc);

			UpdatePodcastInfo(refreshParts);

			await coverResult;

			lock (m_RefreshLock)
			{
				if (m_RefreshPodcastInfo.RefreshNumber == 1)
				{
					Debug.WriteLine($"podcast '{m_RefreshPodcastInfo.Title}' refrehsed");

					//this way if two or more tasks are refreshing same podcast only latest refresh will send event and update things.
					m_Refreshed = true;
					PodcastRefreshed?.Invoke(this, EventArgs.Empty);
				}

				m_RefreshPodcastInfo.RefreshNumber--;
			}
		}

		public PodcastInfo GetPodcastInfo()
		{
			if (!m_HaveCustominfo)
			{
				return m_DefaultPodcastInfo;
			}

			PodcastInfo info = new PodcastInfo();

			info.Title = m_CustomPodcastInfo.Title == "" ? m_DefaultPodcastInfo.Title : m_CustomPodcastInfo.Title;
			info.FeedUrl = m_CustomPodcastInfo.FeedUrl == "" ? m_DefaultPodcastInfo.FeedUrl : m_CustomPodcastInfo.FeedUrl;
			info.CoverUrl = m_CustomPodcastInfo.CoverUrl == "" ? m_DefaultPodcastInfo.CoverUrl : m_CustomPodcastInfo.CoverUrl;
			info.CoverFilePath = m_CustomPodcastInfo.CoverFilePath== "" ? m_DefaultPodcastInfo.CoverFilePath: m_CustomPodcastInfo.CoverFilePath;

			info.Description = m_DefaultPodcastInfo.Description;

			return info;
		}


		private void UpdatePodcastInfo(RefreshParts refreshParts)
		{
			if (refreshParts.HasFlag(RefreshParts.CoverUrl))
			{
				m_DefaultPodcastInfo.CoverUrl = m_RefreshPodcastInfo.CoverUrl;
			}

			if (refreshParts.HasFlag(RefreshParts.Description))
			{
				m_DefaultPodcastInfo.Description = m_RefreshPodcastInfo.Description;
			}

			if (refreshParts.HasFlag(RefreshParts.FeedUrl))
			{
				//m_PodcastInfo.FeedUrl = m_RefreshPodcastInfo.FeedUrl;
			}

			if (refreshParts.HasFlag(RefreshParts.Title))
			{
				m_DefaultPodcastInfo.Title = m_RefreshPodcastInfo.Title;
			}
		}

		private async Task<XDocument> GetRssXdoc()
		{
			string rssString = "";
			var client = new System.Net.WebClient();
			client.Encoding = Encoding.UTF8;

			if (m_DefaultPodcastInfo.FeedUrl == "") throw new RefreshFailedException($"Url was empty in podcast '{m_DefaultPodcastInfo.Title}'", RefreshStatus.NOURL);

			try
			{
				rssString = await client.DownloadStringTaskAsync(m_DefaultPodcastInfo.FeedUrl).ConfigureAwait(false);
			}
			catch (Exception ex)
			{
				throw new RefreshFailedException("Failed to download rss file. Check your url and internet connection.", ex, RefreshStatus.NETWORKERROR);
			}

			var xmlDoc = new XDocument();
			xmlDoc.Declaration = new XDeclaration("1.0", "utf-8", "yes");

			try
			{
				xmlDoc = XDocument.Parse(rssString);
			}
			catch (Exception ex)
			{
				throw new RefreshFailedException("Failed to parse rss file.", ex, RefreshStatus.RSSPARSEERROR);
			}

			return xmlDoc;
		}

		private void GetPodcastInfo(XDocument doc)
		{
			XPathNavigator navi = doc.CreateNavigator();

			var titleNode = navi.SelectSingleNode("rss/channel/title");
			if (titleNode == null) throw new RefreshFailedException("Rss file does not contain title for podcast." , RefreshStatus.INVALIDRSS);

			m_RefreshPodcastInfo.Title = titleNode?.Value ?? "";

			var coverUrlNode = navi.SelectSingleNode("rss/channel/image/url");
			m_RefreshPodcastInfo.CoverUrl = coverUrlNode?.Value ?? "";

			var descriptionNode = navi.SelectSingleNode("rss/channel/description");
			m_RefreshPodcastInfo.Description = descriptionNode?.Value ?? "";
		}

		private void GetEpisodesInfo(XDocument doc)
		{
			//TODO: possibly huge performance hit/long waiting time. well not so huge hit actually
			lock (m_EpisodeLock)
			{
				var newEpisodes = new List<Episode>();
				var currentEpisodes = m_Episodes.ToList();

				XPathNavigator navi = doc.CreateNavigator();

				navi.MoveToFollowing(XPathNodeType.Element);
				var namespaces = navi.GetNamespacesInScope(XmlNamespaceScope.All);

				string itunesNamespace = "";

				if (namespaces.ContainsKey("itunes"))
				{
					itunesNamespace = namespaces["itunes"];
				}

				var items = doc.Element("rss").Element("channel").Elements("item");

				for (int i = 0; i < items.Count(); ++i)
				{
					var item = items.ElementAt(i);

					string eTitle = "";
					string ePublishDate = "";
					string eDescription = "";
					string rawDescription = "";
					string eContentUrl = "";
					string eContentLength = "";
					string eContentType = "";
					string eDuration = "";

					if (item.Element("title") != null) eTitle = item.Element("title").Value;

					if (item.Element("pubDate") != null) ePublishDate = item.Element("pubDate").Value;

					if (item.Element("description") != null) rawDescription = item.Element("description").Value.Trim();
					//eDescription = rawDescription;
					eDescription = Regex.Replace(rawDescription, @"(?></?\w+)(?>(?:[^>'""]+|'[^']*'|""[^""]*"")*)>", String.Empty);

					if (item.Element("enclosure") != null)
					{
						var enclosure = item.Element("enclosure");

						if (enclosure.Attribute("url") != null) eContentUrl = enclosure.Attribute("url").Value;
						if (enclosure.Attribute("length") != null) eContentLength = enclosure.Attribute("length").Value;
						if (enclosure.Attribute("type") != null) eContentType = enclosure.Attribute("type").Value;
					}

					if (itunesNamespace != "")
					{
						var iSummary = item.Element(XName.Get("summary", itunesNamespace));
						var iDuration = item.Element(XName.Get("duration", itunesNamespace));

						if (iSummary != null) eDescription = iSummary.Value;
						if (iDuration != null) eDuration = iDuration.Value;
					}

					//tries to find if episode already exits
					var episodeMatch = currentEpisodes.Where(x => x.AudioUrl == eContentUrl).FirstOrDefault();

					if (episodeMatch != null) break;

					var episode = new Episode();

					Console.WriteLine("new episode: " + eTitle + " ++ " + episode.m_Guid);


					eDescription = eDescription.Replace('<', '[');
					eDescription = eDescription.Replace('>', ']');

					episode.Title = eTitle;
					episode.AudioUrl = eContentUrl;
					episode.Description = eDescription;
					episode.RelaseDate = DateTime.Parse(ePublishDate);

					if (eDuration != "")
					{
						string[] formats = { @"hh\:mm\:ss", @"mm\:ss" };
						TimeSpan result;
						if (TimeSpan.TryParseExact(eDuration, formats, null, out result))
						{
							episode.Duration = result;
						}
					}
					else
					{
						episode.Duration = TimeSpan.Zero;
					}

					newEpisodes.Add(episode);
				}

				m_Episodes.InsertRange(0, newEpisodes);
			}
		}

		private async Task DownloadCoverAsync()
		{
			if (m_RefreshPodcastInfo.CoverUrl == "")
			{
				m_RefreshPodcastInfo.CoverFilePath = "";
				return;
			}

			using (WebClient client = new WebClient())
			{
				var indexOfFirst = m_RefreshPodcastInfo.CoverUrl.LastIndexOf('.');
				var extension = m_RefreshPodcastInfo.CoverUrl.Substring(indexOfFirst);
				var filename = Utils.GetPodcastFilename(this) + extension;

				try
				{
					Debug.WriteLine("Started getting file: " + filename);

					var data = await client.DownloadDataTaskAsync(m_RefreshPodcastInfo.CoverUrl).ConfigureAwait(false);

					Utils.SafeFileWrite(new Uri("podcasts/" + filename, UriKind.Relative), () =>
					{
						File.WriteAllBytes("podcasts/" + filename, data);
					});

					Debug.WriteLine("got cover " + filename);

				}
				catch (WebException ex)
				{
					throw new RefreshFailedException("Failed to get cover.", ex, RefreshStatus.NETWORKERROR);
				}

				m_RefreshPodcastInfo.CoverFilePath = "podcasts/" + filename;
			}
		}
	}
}
