﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Windows;
using System.Diagnostics;

namespace Loiste.src
{
	public class Utils
	{
		private static ConcurrentDictionary<Uri, ReaderWriterLockSlim> fileLocks = new ConcurrentDictionary<Uri, ReaderWriterLockSlim>();

		public static string ToShortForm(TimeSpan t)
		{
			string shortForm = "";
			if (t.Hours > 0)
			{
				shortForm += string.Format("{0} h", t.Hours.ToString());
				shortForm += " ";
			}
			if (t.Minutes > 0)
			{
				shortForm += string.Format("{0} min", t.Minutes.ToString());
			}
			return shortForm;
		}

		public static bool TestImageValidity(Uri imageUri)
		{
			try
			{
				BitmapImage testImage = new BitmapImage();

				testImage.BeginInit();

				SafeFileRead(imageUri, () =>
				{
					testImage.UriSource = imageUri;
				});

				testImage.EndInit();
			}
			catch
			{
				Debug.WriteLine($"file '{imageUri}' is not valid");
				return false;
			}

			return true;
		} 


		public static BitmapImage SafeBitmapGet(string imagePath, Point? decodeSize = null)
		{
			BitmapImage logo = new BitmapImage();

			try
			{
				logo.BeginInit();
				logo.CacheOption = BitmapCacheOption.OnLoad;

				if (decodeSize.HasValue)
				{
					logo.DecodePixelHeight = (int)decodeSize.Value.Y;
					logo.DecodePixelWidth = (int)decodeSize.Value.X;
				}

				bool imageExists = imagePath != "" ? File.Exists(imagePath) : false;
				bool imageValid = false;

				if (imageExists)
				{
					imageValid = TestImageValidity(new Uri(imagePath, UriKind.Relative));
				}
				else
				{
					Debug.WriteLine($"could not find file '{imagePath}'");
				}

				if (imageExists && imageValid)
				{
					SafeFileRead(new Uri(imagePath, UriKind.Relative), () =>
					{
						logo.UriSource = new Uri(imagePath, UriKind.Relative);
					});
				}
				else
				{
					SafeFileRead(new Uri("pack://application:,,,/Resources/nocover.png"), () =>
					{
						logo.UriSource = new Uri("pack://application:,,,/Resources/nocover.png");
					});
				}

				logo.EndInit();
			}
			catch (Exception ex)
			{
				//TDOO: fix notsupportedimage exception
			}
			
			return logo;
		}

		public static void SafeFileRead(Uri uri, Action action)
		{
			var _lock = fileLocks.GetOrAdd(uri, s => new ReaderWriterLockSlim());

			try
			{
				_lock.EnterReadLock();

				action();
			}
			finally
			{
				_lock.ExitReadLock();
			}
		}

		public static void SafeFileWrite(Uri uri, Action action)
		{
			var _lock = fileLocks.GetOrAdd(uri, s => new ReaderWriterLockSlim());

			try
			{
				_lock.EnterWriteLock();

				action();
			}
			finally
			{
				_lock.ExitWriteLock();
			}

		}

		public static string GetPodcastFilename(Podcast podcast)
		{
			return podcast.GetPodcastInfo().Title.Replace(" ", "_");
		}

		public static void UpdateVisuals(Action action)
		{
			//Action updateInfo = () =>
			//{
				
			//};

			//if (CheckAccess())
			//{
			//	updateInfo();
			//}
			//else
			//{
			//	Dispatcher.BeginInvoke(updateInfo);
			//}
		}

	}
}
