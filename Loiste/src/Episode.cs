﻿using Loiste.src;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Loiste
{
	[JsonObject(MemberSerialization.OptOut)]
	public class Episode
	{
		public string Title = "";

		public DateTime RelaseDate;

		public string Description = "";

		public string AudioUrl = "";

		public TimeSpan Duration;

		public bool m_Listened; //listened over 50%

		public Guid m_Guid;


		public Episode()
		{
			m_Guid = Guid.NewGuid();
		}

	}
}
