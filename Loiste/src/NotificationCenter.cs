﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loiste.src
{
	public class NotificationCenter
	{
		public List<string> m_Notifications = new List<string>();

		public void PushNotification(string message)
		{
			m_Notifications.Add(message);
		}
	}
}
