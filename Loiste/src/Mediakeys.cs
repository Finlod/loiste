﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Interop;

namespace Loiste.src
{
	class Mediakeys : IDisposable
	{
		[DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		private static extern IntPtr SetWindowsHookEx(int idHook, LowLevelKeyboardProc lpfn, IntPtr hMod, int dwThreadId);

		[DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		private static extern bool UnhookWindowsHookEx(IntPtr hhk);

		[DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		private static extern IntPtr CallNextHookEx(IntPtr hhk, int nCode, IntPtr wParam, IntPtr lParam);

		[DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		private static extern IntPtr GetModuleHandle(string lpModuleName);

		private const int VK_MEDIA_NEXT_TRACK = 0xB0;
		private const int VK_MEDIA_PREV_TRACK = 0xB1;
		private const int VK_MEDIA_STOP = 0xB2;
		private const int VK_MEDIA_PLAY_PAUSE = 0xB3;
					 
		private const int WH_KEYBOARD_LL = 13;
		private const int WM_KEYDOWN = 0x0100;

		public event EventHandler NextTrackPressed;
		public event EventHandler PrevTrackPressed;
		public event EventHandler StopPressed;
		public event EventHandler PlayPausePressed;

		private delegate IntPtr LowLevelKeyboardProc(int nCode, IntPtr wParam, IntPtr lParam);

		private static LowLevelKeyboardProc lowLevelKeyboardProc;
		private IntPtr hHook = IntPtr.Zero;
		private IntPtr hwnd = IntPtr.Zero;

		public Mediakeys(Window window)
		{
			Debug.Assert(window != null);

			var windowHelper = new WindowInteropHelper(window);
			hwnd = windowHelper.Handle;

			Debug.Assert(hwnd.ToInt32() != 0, "WindowInteropHelper handle has not yet been created. Earliest place where it is created is OnSourceInitialized.");

			if (lowLevelKeyboardProc != null)
			{
				Console.WriteLine("No two instances of Mediakeys can be alive at the same time.");
				return;
			}

			lowLevelKeyboardProc = new LowLevelKeyboardProc(LowLevelKeyboardHookCallback);

			using (Process curProcess = Process.GetCurrentProcess())
			using (ProcessModule curModule = curProcess.MainModule)
			{
				hHook = SetWindowsHookEx(WH_KEYBOARD_LL, lowLevelKeyboardProc, GetModuleHandle(curModule.ModuleName), 0);

				Debug.Assert(hHook != IntPtr.Zero, "setting windows hook failed.");
			}
		}

		~Mediakeys()
		{
			Dispose (false);
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		protected virtual void Dispose(bool disposing)
		{
			if (!UnhookWindowsHookEx(hHook))
			{
				Console.WriteLine("unhooking windows failed.");

				string errorMessage = new Win32Exception(Marshal.GetLastWin32Error()).Message;
				Console.WriteLine(errorMessage);
			}

			lowLevelKeyboardProc = null;
		}

		public IntPtr LowLevelKeyboardHookCallback(int nCode, IntPtr wParam, IntPtr lParam)
		{
			if (nCode >= 0 && wParam == (IntPtr)WM_KEYDOWN)
			{
				int vkCode = Marshal.ReadInt32(lParam);
				
				switch (vkCode)
				{
					case VK_MEDIA_NEXT_TRACK:
						if (NextTrackPressed != null) NextTrackPressed(this, EventArgs.Empty);
						break;
					case VK_MEDIA_PREV_TRACK:
						if (PrevTrackPressed != null) PrevTrackPressed(this, EventArgs.Empty);
						break;
					case VK_MEDIA_STOP:
						if (StopPressed != null) StopPressed(this, EventArgs.Empty);
						break;
					case VK_MEDIA_PLAY_PAUSE:
						if (PlayPausePressed != null) PlayPausePressed(this, EventArgs.Empty);
						break;
					default:
						break;
				}


			}
			
			return CallNextHookEx(hHook, nCode, wParam, lParam); 
		}
	}
}
