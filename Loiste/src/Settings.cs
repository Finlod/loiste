﻿using Loiste.src;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loiste
{
	[JsonObject(MemberSerialization.OptOut)]
	public class Settings
	{
		//-----Audio-----

		public bool m_LoadMFOnStartup;
		public bool m_EnableMF;
		public bool m_SessionVolume;
		public float m_DefaultVolume;

		//-----General-----

		public bool m_LoadSessionInfo;
		public bool m_EnableMediakeys;

		public bool m_RefreshOnStartup;
		public bool m_DeepRefresh;

		//public bool m_LowRam;

	}
}
