﻿using Loiste.src;
using MediaFoundation;
using MediaFoundation.Misc;
using System;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;

namespace Loiste
{
	public class EpisodeEventArgs : EventArgs
	{
		public Episode m_Episode;

		public EpisodeEventArgs(Episode episode)
		{
			m_Episode = episode;
		}

	}

	public class ErrorEventArgs : EventArgs
	{
		public AudioErrorCode m_ErrorCode;

		public ErrorEventArgs(AudioErrorCode errorCode)
		{
			m_ErrorCode = errorCode;
		}

	}

	public enum AudioErrorCode
	{
		Unknown = -1,
		NoSourceFound
	}

	

	public class Metadata
	{
		public string TrackTitle;
		public string Author;
		//public string Comment;
		//public string Keywords;
		public DateTime ReleaseDate;
	}

	public class AudioHandler : COMBase, IMFAsyncCallback, IDisposable
	{
		private float? volumeBeforeMute = null;
		private float volume = 1.0f;
		private long duration;
		private TimeSpan startingTime;
		private bool startImmediately;
		private TimeSpan seekTime;
		private bool initialized;

		//When we start to get audio from internet / setting up topologies.
		public event EventHandler BufferingStarted;
		public event EventHandler TryingToPlay;
		public event EventHandler ReadyToPlay;
		public event EventHandler PlaybackStarted;
		public event EventHandler<ErrorEventArgs> ErrorEvent;
		//public event EventHandler PlaybackResumed;
		public event EventHandler PlaybackPaused;
		public event EventHandler PlaybackStopped;
		public event EventHandler PlaybackEnded;

		public event EventHandler EpisodeChanged;

		public event EventHandler VolumeChanged;

		private Episode m_CurrentEpisode;
		public Episode CurrentEpisode
		{
			get 
			{ 
				return m_CurrentEpisode;
			}
			private set
			{
				var lastEpisode = m_CurrentEpisode;
				m_CurrentEpisode = value;
				if (value != lastEpisode) EpisodeChanged(this, EventArgs.Empty);
			}
		}

		public Podcast CurrentPodcast
		{
			get;
			private set;
		}

		public AudioState.PlaybackState PlaybackState
		{
			get;
			private set;
		}

		private IMFMediaSession pSession;
		private IMFMediaSource pSource;
		private IMFAudioStreamVolume pAudioVolume;
		private MFSessionCapabilities sCaps;
		private IMFClock pClock;

		private AutoResetEvent closeEvent;
		private System.Timers.Timer listenedTimer;

		const int MF_VERSION = 0x10070;
		

		public AudioHandler()
		{
			initialized = false;

			pSession = null;
			pSource = null;

			PlaybackState = AudioState.PlaybackState.Stopped;

			closeEvent = new AutoResetEvent(false);
			listenedTimer = new System.Timers.Timer();
			listenedTimer.Interval = 60000; //60 secs
			listenedTimer.Elapsed += new System.Timers.ElapsedEventHandler(OnListenedTimerElapsed);
		}

		~AudioHandler()
		{
			Dispose (false);
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		protected virtual void Dispose(bool disposing)
		{
			Shutdown();

			if (disposing)
			{
				closeEvent.Dispose();
				listenedTimer.Dispose();
			}
		}


		private void OnListenedTimerElapsed(object sender, System.Timers.ElapsedEventArgs e)
		{
			CheckEpisodeListened();
		}

		private void CheckEpisodeListened()
		{
			if (GetPosition().TotalSeconds >= GetDuration().TotalSeconds / 2) //if over 50% listened.
			{
				CurrentEpisode.m_Listened = true;
			}
		}


		public void Initialize(bool forze = false)
		{
			if (forze || !initialized)
			{
				int hr = MFExtern.MFStartup(MF_VERSION, MFStartup.Full);
				MFError.ThrowExceptionForHR(hr);
			}

			initialized = true;
		}

		public void Shutdown()
		{
			if (initialized && closeEvent != null)
			{
				CloseSession();

				int hr = MFExtern.MFShutdown();
				MFError.ThrowExceptionForHR(hr);

				closeEvent.Close();
				closeEvent = null;

				Debug.WriteLine("Media foundation shutdown.");
			}

			initialized = false;
		}


		public void Download(Episode episode)
		{
			//var sourceState = episode.AudioState.m_SourceState;

			//switch (sourceState)
			//{
			//	case AudioState.SourceState.Downloaded:
			//		Debug.WriteLine("Episode '" + episode.Title + "' already downloaded.");
			//		break;
			//	case AudioState.SourceState.Downloading:
			//		Debug.WriteLine("Already downloading episode '" + episode.Title + "'.");
			//		break;
			//	case AudioState.SourceState.DownloadPaused:
			//		//TODO: continue download
			//		break;
			//	case AudioState.SourceState.Streaming:
			//		//TODO: convert stream to download
			//	case AudioState.SourceState.NotFound:
			//		var audiostream = new AudioStream(episode, false);
			//		PlaybackState = AudioState.PlaybackState.Stopped;
			//		audioStreams.Add(Tuple.Create(audiostream, episode));

			//		ThreadPool.QueueUserWorkItem(audiostream.GetAudio);

			//		episode.AudioState.m_SourceState = AudioState.SourceState.Downloading;
			//		break;
			//}
		}


		public void Play(Podcast podcast, Episode episode, bool startImmediately = true, TimeSpan? position = null)
		{
			var sourceState = AudioState.SourceState.NotFound;//episode.AudioState.m_SourceState;
			var lastEpisode = CurrentEpisode;
			this.startImmediately = startImmediately;

			if (position.HasValue)
			{
				startingTime = position.Value;
			}
			else
			{
				startingTime = TimeSpan.Zero;
			}

			CurrentPodcast = podcast;
			CurrentEpisode = episode;

			if (!initialized) Initialize();

			if (TryingToPlay != null) TryingToPlay(this, EventArgs.Empty);

			switch (sourceState)
			{
				case AudioState.SourceState.Downloaded:
					break;
				case AudioState.SourceState.Downloading:
					//Check if ready to play
					break;
				case AudioState.SourceState.DownloadPaused:
					break;
				case AudioState.SourceState.Streaming:
					if (lastEpisode != episode)
					{
						//
						Console.WriteLine("Error, source is streaming but episode doesn't match");
					}
					else
					{
						//CurrentEpisode already set
						Play();
					}
					break;
				case AudioState.SourceState.NotFound:
					GetAudio(episode.AudioUrl);

					break;
			}
		}

		public void Play()
		{
			if (PlaybackState == AudioState.PlaybackState.Playing) return;

			if (pSession == null || pSource == null) return;

			try
			{
				pSession.Start(Guid.Empty, new PropVariant());
			}
			catch (Exception ex)
			{
				SendErrorEvent(ex);
			}
		}

		public void Pause()
		{
			if (PlaybackState != AudioState.PlaybackState.Playing) return;

			if (pSession == null || pSource == null) return;

			try
			{
				int hr = pSession.Pause();
				MFError.ThrowExceptionForHR(hr);
			}
			catch (Exception ex)
			{
				SendErrorEvent(ex);
			}
		}

		public void PlayPause()
		{
			if (PlaybackState == AudioState.PlaybackState.Playing)
			{
				Pause();
			}
			else if (PlaybackState == AudioState.PlaybackState.Paused)
			{
				Play();
			}
		}

		public void Stop()
		{
			if (PlaybackState != AudioState.PlaybackState.Playing || PlaybackState != AudioState.PlaybackState.Paused) return;

			if (pSession == null || pSource == null) return;

			try
			{
				int hr = pSession.Stop();
				MFError.ThrowExceptionForHR(hr);
			}
			catch (Exception ex)
			{
				SendErrorEvent(ex);
			}
		}

		/*
		 * Volume is expressed as an attenuation level, where 0.0 indicates silence and 1.0 indicates full volume (no attenuation). For each channel, the attenuation level is the product of:
		 * The master volume level of the audio session.
		 * The volume level of the channel.
		 * For example, if the master volume is 0.8 and the channel volume is 0.5, the attenuation for that channel is 0.8 × 0.5 = 0.4. Volume levels can exceed 1.0 (positive gain), but the audio engine clips any audio samples that exceed zero decibels.
		 * Use the following formula to convert the volume level to the decibel (dB) scale:
		 * Attenuation (dB) = 20 * log10(Level)
		 * For example, a volume level of 0.50 represents 6.02 dB of attenuation.
		*/
		public void SetVolume(float volume)
		{
			int hr = S_Ok;
			int channelCount = 2;

			volume = Math.Max(0, volume);
			volume = Math.Min(1, volume);

			this.volume = volume;

			if (pAudioVolume == null) return;

			hr = pAudioVolume.GetChannelCount(out channelCount);
			MFError.ThrowExceptionForHR(hr);

			float[] volumes = new float[channelCount];

			Console.WriteLine("new volume " + volume);

			for (int i = 0; i < channelCount; ++i) volumes[i] = volume;

			hr = pAudioVolume.SetAllVolumes(channelCount, volumes);
			MFError.ThrowExceptionForHR(hr);
			
			if (VolumeChanged != null) VolumeChanged(this, EventArgs.Empty);
		}

		public float GetVolume()
		{
			return volume;
		}

		public void Mute()
		{
			SetVolume(0.0f);

			volumeBeforeMute = volume;
		}

		public void UnMute()
		{
			if (!volumeBeforeMute.HasValue)
				SetVolume(volumeBeforeMute.Value);
		}

		
		public TimeSpan GetDuration()
		{
			return new TimeSpan(duration);
		}

		public TimeSpan GetPosition()
		{
			long clockTime = 0;
			long systemTime;

			if (pSession == null || pSource == null) return TimeSpan.Zero;

			try
			{
				int hr = pSession.GetClock(out pClock);
				MFError.ThrowExceptionForHR(hr);

				hr = pClock.GetCorrelatedTime(0, out clockTime, out systemTime);
			}
			catch (Exception ex)
			{
				SendErrorEvent(ex);
			}

			return new TimeSpan(clockTime);// + startingTime.Ticks); //we could get offset from MESessionNotifyPresentationTime event, but there shouldn't be any difference.
		}

		/**
		 * If audio playing, will continue to play, else audio is paused.
		*/
		public void Seek(TimeSpan toTime)
		{
			int hr = S_Ok;
			PropVariant varStart = new PropVariant(toTime.Ticks);// - startingTime.Ticks);

			Debug.Assert(pSession != null, "cannot seek, pSession is null");

			if (!CanSeek())
			{
				Console.WriteLine("Stream don't support seeking!");
				return;
			}

			seekTime = toTime;

			try
            {
				hr =  pSession.Start(Guid.Empty, varStart);
				MFError.ThrowExceptionForHR(hr);

				if (PlaybackState != AudioState.PlaybackState.Playing) pSession.Pause();
            }
            catch (Exception ex)
            {
				SendErrorEvent(ex, AudioErrorCode.Unknown, true);
            }
            finally
            {
				varStart.Clear();
            }	
		}

		public bool CanSeek()
		{
			return ((sCaps & MFSessionCapabilities.Seek) == MFSessionCapabilities.Seek) && (duration > 0) && (pClock != null);
		}

		/**
		 * Wont work for streams.
		 * Do not use
		 * Returns filled Metadata object, if fails will return null.
		*/
		public Metadata GetShellmetadata()
		{
			Metadata metadata = null;
			IPropertyStore pProps = null;
			object tmpProps;

			if (pSession == null || pSource == null)
			{
				Debug.WriteLine("pSession or pSource is null, cannot get metadata.");
				return null;
			}

			int hr = 0;

			try
			{
				hr = MFExtern.MFGetService(pSource, MFServices.MF_PROPERTY_HANDLER_SERVICE, typeof(IPropertyStore).GUID, out tmpProps);
				MFError.ThrowExceptionForHR(hr);

				pProps = (IPropertyStore)tmpProps;

				int numProps;

				hr = pProps.GetCount(out numProps);
				MFError.ThrowExceptionForHR(hr);

				for (int i = 0; i < numProps; ++i)
				{
					PropertyKey key = new PropertyKey();
					PropVariant variant = new PropVariant();

					hr = pProps.GetAt(i, key);
					MFError.ThrowExceptionForHR(hr);

					hr = pProps.GetValue(key, variant);
					MFError.ThrowExceptionForHR(hr);

					AddToShellMetadata(key, variant, ref metadata);
				}
			} 
			catch (Exception ex)
			{
				if (hr == MFError.MF_E_UNSUPPORTED_SERVICE)
				{
					Debug.WriteLine("No support for metadata.");
				}
				else
				{
					SendErrorEvent(ex, AudioErrorCode.Unknown, true);
				}
			}
			finally
			{
				SafeRelease(pProps);
			}

			return metadata;
		}
		
		private void AddToShellMetadata(PropertyKey key, PropVariant variant, ref Metadata metadata)
		{
			throw new NotImplementedException();
		}


		public Metadata GetMetadata()
		{
			Metadata metadata = null;
			IMFPresentationDescriptor pPD = null;
			IMFMetadataProvider pProvider = null;
			IMFMetadata pMetadata = null;
			PropVariant varNames = new PropVariant();
			object tmpProvider;

			if (pSession == null || pSource == null)
			{
				Debug.WriteLine("pSession or pSource is null, cannot get metadata.");
				return null;
			}

			try
			{
				int hr = pSource.CreatePresentationDescriptor(out pPD);
				MFError.ThrowExceptionForHR(hr);

				hr = MFExtern.MFGetService(pSource, MFServices.MF_METADATA_PROVIDER_SERVICE, typeof(IMFMetadataProvider).GUID, out tmpProvider);
				MFError.ThrowExceptionForHR(hr);

				pProvider = (IMFMetadataProvider)tmpProvider;

				//presenatation-level metadata
				int streamId = 0;

				hr = pProvider.GetMFMetadata(pPD, streamId, 0, out pMetadata);
				MFError.ThrowExceptionForHR(hr);

				hr = pMetadata.GetAllPropertyNames(varNames);
				MFError.ThrowExceptionForHR(hr);

				if (varNames.GetVariantType() != PropVariant.VariantType.StringArray) return null; //no metadata

				var metaString = varNames.GetStringArray();

				metadata = new Metadata();

				for (int i = 0; i < metaString.Count(); ++i)
				{
					PropVariant varValue = new PropVariant();

					hr = pMetadata.GetProperty(metaString[i], varValue);
					MFError.ThrowExceptionForHR(hr);

					AddToMetadata(metaString[i], varValue.ToString(), ref metadata);

				}

			}
			catch (Exception ex)
			{
				SendErrorEvent(ex, AudioErrorCode.Unknown, true);
			}
			finally
			{
				SafeRelease(pPD);
				SafeRelease(pProvider);
				SafeRelease(pMetadata);
			}

			return metadata;
		}

		private void AddToMetadata(string propertyName, string varValue, ref Metadata metadata)
		{
			switch (propertyName)
			{
				case "Title":
					{
						metadata.TrackTitle = varValue;
						break;
					}
				case "WM/Year":
					{
						DateTime datetime;

						if (metadata.ReleaseDate == null)
						{
							datetime = new DateTime();
						}
						else
						{
							datetime = metadata.ReleaseDate;
						}

						int year = Int32.Parse(varValue);

						metadata.ReleaseDate = new DateTime(year, datetime.Month, datetime.Day, datetime.Hour, datetime.Minute, 0);

						break;
					}
				case "Author":
					{
						metadata.Author = varValue;
						break;
					}
				case "ID3/TDAT":
					{
						DateTime datetime;

						if (metadata.ReleaseDate == null)
						{
							datetime = new DateTime();
						}
						else
						{
							datetime = metadata.ReleaseDate;
						}

						var _datetime = DateTime.ParseExact(varValue, "ddMM", CultureInfo.InvariantCulture);

						metadata.ReleaseDate = new DateTime(datetime.Year, _datetime.Month, _datetime.Day, datetime.Hour, datetime.Minute, 0);

						break;
					}
				case "ID3/TIME":
					{
						DateTime datetime;

						if (metadata.ReleaseDate == null)
						{
							datetime = new DateTime();
						}
						else
						{
							datetime = metadata.ReleaseDate;
						}

						var _datetime = DateTime.ParseExact(varValue, "HHmm", CultureInfo.InvariantCulture);

						metadata.ReleaseDate = new DateTime(datetime.Year, datetime.Month, datetime.Day, _datetime.Hour, _datetime.Minute, 0);

						break;
					}
			}
		}


		private void SendErrorEvent(string errorMessage, AudioErrorCode errorCode = AudioErrorCode.Unknown, bool fail = false)
		{
			if (fail)
			{
				Debug.Fail(errorMessage);
			}
			else
			{
				Debug.WriteLine(errorMessage);
			}

			if (ErrorEvent != null) ErrorEvent(this, new ErrorEventArgs(errorCode));
		}

		private void SendErrorEvent(Exception exception, AudioErrorCode errorCode = AudioErrorCode.Unknown, bool fail = false)
		{
			SendErrorEvent(exception.Message, errorCode, fail);
		}


		private void GetAudio(string url)
		{
			try
			{
				IMFTopology pTopology = null;

				CreateSession();
				CreateMediaSource(url);
				CreateTopologyFromSource(out pTopology);

				int hr = pSession.SetTopology(0, pTopology);
				MFError.ThrowExceptionForHR(hr);

				SafeRelease(pTopology);
			}
			catch (Exception ex)
			{
				if (ex.HResult == -2147024894)
				{
					SendErrorEvent(ex, AudioErrorCode.NoSourceFound);
				}
				else if (ex.HResult == -2147024891)
				{
					SendErrorEvent(ex, AudioErrorCode.NoSourceFound);
				}
				else
				{
					SendErrorEvent(ex, AudioErrorCode.Unknown, true);
				}
			}
		}

		private void StartPlaying()
		{
			int hr = S_Ok;
			PropVariant varStart = new PropVariant(startingTime.Ticks);

			Debug.Assert(pSession != null, "cannot start playing, pSession is null");

			try
			{
				hr = pSession.Start(Guid.Empty, varStart);
				MFError.ThrowExceptionForHR(hr);

				if (!startImmediately) pSession.Pause();
			}
			catch (Exception ex)
			{
				SendErrorEvent(ex, AudioErrorCode.Unknown, true);
			}
			finally
			{
				varStart.Clear();
			}	
		}


		private void CreateSession()
		{
			// Close the old session, if any.
			CloseSession();

			// Create the media session.
			int hr = MFExtern.MFCreateMediaSession(null, out pSession);
			MFError.ThrowExceptionForHR(hr);

			// Start pulling events from the media session
			hr = pSession.BeginGetEvent(this, null);
			MFError.ThrowExceptionForHR(hr);
		}

		private void CloseSession()
		{
			int hr;

			if (pClock != null)
			{
				SafeRelease(pClock);
				pClock = null;
			}

			if (pAudioVolume != null)
			{
				SafeRelease(pAudioVolume);
				pAudioVolume = null;
			}

			if (pSession != null)
			{
				hr = pSession.Close();
				MFError.ThrowExceptionForHR(hr);

				bool result = closeEvent.WaitOne(5000, true);
				if (!result)
					SendErrorEvent("Closing session timed out");
			}

			if (pSource != null)
			{
				hr = pSource.Shutdown();

				MFError.ThrowExceptionForHR(hr);
				SafeRelease(pSource);

				pSource = null;
			}	

			if (pSession != null)
			{
				hr = pSession.Shutdown();

				MFError.ThrowExceptionForHR(hr);
				Marshal.ReleaseComObject(pSession);

				pSession = null;
			}
		}


		private void CreateMediaSource(string url)
		{
			IMFSourceResolver pSourceResolver;
			object _pSource;

			int hr = MFExtern.MFCreateSourceResolver(out pSourceResolver);
			MFError.ThrowExceptionForHR(hr);

			try
			{
				MFObjectType objectType = MFObjectType.Invalid;

				hr = pSourceResolver.CreateObjectFromURL(url, MFResolution.MediaSource, null, out objectType, out _pSource);
				MFError.ThrowExceptionForHR(hr);

				pSource = (IMFMediaSource)_pSource;
			}
			finally
			{
				SafeRelease(pSourceResolver);
			}
		}

		private void CreateTopologyFromSource(out IMFTopology ppTopology)
		{
			IMFTopology pTopology = null;
			IMFPresentationDescriptor pSourcePD = null;
			long _duration;
			int cSourceStreams = 0;
			int hr;

			Debug.Assert(pSession != null);
			Debug.Assert(pSource != null);

			try
			{
				hr = MFExtern.MFCreateTopology(out pTopology);
				MFError.ThrowExceptionForHR(hr);

				hr = pSource.CreatePresentationDescriptor(out pSourcePD);
				MFError.ThrowExceptionForHR(hr);
				
				hr = pSourcePD.GetUINT64(MFAttributesClsid.MF_PD_DURATION, out _duration);
				MFError.ThrowExceptionForHR(hr);

				duration = _duration;

				hr = pSourcePD.GetStreamDescriptorCount(out cSourceStreams);
				MFError.ThrowExceptionForHR(hr);

				Console.WriteLine(string.Format("Stream count: {0}", cSourceStreams));

				for (int i = 0; i < cSourceStreams; i++)
				{
					AddBranchToPartialTopology(pTopology, pSourcePD, i);
				}

				ppTopology = pTopology;
			}
			catch
			{
				SafeRelease(pTopology);
				throw;
			}
			finally
			{
				SafeRelease(pSourcePD);
			}
		}

		private void AddBranchToPartialTopology(IMFTopology pTopology, IMFPresentationDescriptor pSourcePD, int iStream)
		{
			IMFStreamDescriptor pSourceSD = null;
			IMFTopologyNode pSourceNode = null;
			IMFTopologyNode pOutputNode = null;
			bool fSelected = false;
			int hr;

			Debug.Assert(pTopology != null);

			try
			{
				hr = pSourcePD.GetStreamDescriptorByIndex(iStream, out fSelected, out pSourceSD);
				MFError.ThrowExceptionForHR(hr);

				if (fSelected)
				{
					CreateSourceStreamNode(pSourcePD, pSourceSD, out pSourceNode);
					CreateOutputNode(pSourceSD, out pOutputNode);

					//pSourceNode.SetUINT64(MFAttributesClsid.MF_TOPONODE_MEDIASTART, startingTime.Ticks);

					hr = pTopology.AddNode(pSourceNode);
					MFError.ThrowExceptionForHR(hr);

					hr = pTopology.AddNode(pOutputNode);
					MFError.ThrowExceptionForHR(hr);

					hr = pSourceNode.ConnectOutput(0, pOutputNode, 0);
					MFError.ThrowExceptionForHR(hr);
				}
			}
			finally
			{
				SafeRelease(pSourceSD);
				SafeRelease(pSourceNode);
				SafeRelease(pOutputNode);
			}
		}

		private void CreateSourceStreamNode(IMFPresentationDescriptor pSourcePD, IMFStreamDescriptor pSourceSD, out IMFTopologyNode ppNode)
		{
			IMFTopologyNode pNode = null;
			int hr;
			
			Debug.Assert(pSource != null);

			try
			{
				hr = MFExtern.MFCreateTopologyNode(MFTopologyType.SourcestreamNode, out pNode);
				MFError.ThrowExceptionForHR(hr);

				hr = pNode.SetUnknown(MFAttributesClsid.MF_TOPONODE_SOURCE, pSource);
				MFError.ThrowExceptionForHR(hr);

				hr = pNode.SetUnknown(MFAttributesClsid.MF_TOPONODE_PRESENTATION_DESCRIPTOR, pSourcePD);
				MFError.ThrowExceptionForHR(hr);

				hr = pNode.SetUnknown(MFAttributesClsid.MF_TOPONODE_STREAM_DESCRIPTOR, pSourceSD);
				MFError.ThrowExceptionForHR(hr);

				ppNode = pNode;
			}
			catch
			{
				SafeRelease(pNode);
				throw;
			}
		}

		private void CreateOutputNode(IMFStreamDescriptor pSourceSD, out IMFTopologyNode ppNode)
		{
			IMFTopologyNode pNode = null;
			IMFMediaTypeHandler pHandler = null;
			IMFActivate pRendererActivate = null;
			Guid guidMajorType = Guid.Empty;
			int hr = S_Ok;
			int streamID = 0;

			try
			{
				try
				{
					hr = pSourceSD.GetStreamIdentifier(out streamID);
					MFError.ThrowExceptionForHR(hr);
				}
				catch
				{
					SendErrorEvent("Getting stream identifier failed with code: " + hr.ToString());
				}

				hr = pSourceSD.GetMediaTypeHandler(out pHandler);
				MFError.ThrowExceptionForHR(hr);

				hr = pHandler.GetMajorType(out guidMajorType);
				MFError.ThrowExceptionForHR(hr);

				hr = MFExtern.MFCreateTopologyNode(MFTopologyType.OutputNode, out pNode);
				MFError.ThrowExceptionForHR(hr);

				if (MFMediaType.Audio == guidMajorType)
				{
					Console.WriteLine(string.Format("Stream {0}: audio stream", streamID));

					hr = MFExtern.MFCreateAudioRendererActivate(out pRendererActivate);
					MFError.ThrowExceptionForHR(hr);
				}
				else
				{
					Console.WriteLine(string.Format("Stream {0}: Unknown format", streamID));

					throw new COMException("Unknown format", E_Fail);
				}

				hr = pNode.SetObject(pRendererActivate);
				MFError.ThrowExceptionForHR(hr);

				ppNode = pNode;
			}
			catch
			{
				SafeRelease(pNode);
				throw;
			}
			finally
			{
				SafeRelease(pHandler);
				SafeRelease(pRendererActivate);
			}
		}


		private void OnTopologyReady(IMFMediaEvent pEvent)
		{
			object pStreamVolume;

			int hr = pSession.GetSessionCapabilities(out sCaps);
			MFError.ThrowExceptionForHR(hr);

			 hr = MFExtern.MFGetService(pSession, MFServices.MR_STREAM_VOLUME_SERVICE, typeof(IMFAudioStreamVolume).GUID, out pStreamVolume);
			MFError.ThrowExceptionForHR(hr);

			pAudioVolume = pStreamVolume as IMFAudioStreamVolume;

			hr = pSession.GetClock(out pClock);
			MFError.ThrowExceptionForHR(hr);

			SetVolume(this.volume);

			//Need to be send after we have IMFAudioStreamVolume and clock, but before calling Play(). Otherwise there is theoretical chance PlaybackStarted is called before ReadyToPlay.
			if (ReadyToPlay != null) ReadyToPlay(this, EventArgs.Empty);

			StartPlaying();
		}

		private void OnSessionStarted(IMFMediaEvent pEvent)
		{
			PlaybackState = AudioState.PlaybackState.Playing;

			listenedTimer.Start();

			if (PlaybackStarted != null) PlaybackStarted(this, new EpisodeEventArgs(CurrentEpisode));
		}

		private void OnSessionPaused(IMFMediaEvent pEvent)
		{
			PlaybackState = AudioState.PlaybackState.Paused;

			listenedTimer.Stop();
			CheckEpisodeListened();

			if (PlaybackPaused != null) PlaybackPaused(this, EventArgs.Empty);
		}

		private void OnSessionStopped(IMFMediaEvent pEvent)
		{
			PlaybackState = AudioState.PlaybackState.Stopped;

			listenedTimer.Stop();
			CheckEpisodeListened();

			if (PlaybackStopped != null) PlaybackStopped(this, EventArgs.Empty);
		}

		private void OnSessionClosed(IMFMediaEvent pEvent)
		{
			OnSessionStopped(null);

			closeEvent.Set();
		}

		private void OnSessionEnded(IMFMediaEvent pEvent)
		{
			CurrentEpisode = null;

			PlaybackState = AudioState.PlaybackState.Ended;

			listenedTimer.Stop();
			CheckEpisodeListened();

			if (PlaybackEnded != null) PlaybackEnded(this, EventArgs.Empty);
		}

		private void OnBufferingStarted(IMFMediaEvent pEvent)
		{
			PlaybackState = AudioState.PlaybackState.Buffering;

			if (BufferingStarted != null) BufferingStarted(this, EventArgs.Empty);
		}

		private void OnBufferingStopped(IMFMediaEvent pEvent)
		{
		}


		private void OnPresentationEnded(IMFMediaEvent pEvent)
		{
			Console.WriteLine("CPlayer::OnPresentationEnded");
		}


		int IMFAsyncCallback.GetParameters(out MFASync pdwFlags, out MFAsyncCallbackQueue pdwQueue)
		{
			pdwFlags = MFASync.FastIOProcessingCallback;
			pdwQueue = MFAsyncCallbackQueue.Standard;

			return S_Ok;
		}

		int IMFAsyncCallback.Invoke(IMFAsyncResult pResult)
		{
			IMFMediaEvent pEvent = null;
			MediaEventType eventType = MediaEventType.MEUnknown;
			MFTopoStatus topologyStatus = MFTopoStatus.Invalid;
			int hr;
			int eventStatus = 0;

			try
			{
				hr = pSession.EndGetEvent(pResult, out pEvent);
				MFError.ThrowExceptionForHR(hr);

				hr = pEvent.GetType(out eventType);
				MFError.ThrowExceptionForHR(hr);

				hr = pEvent.GetStatus(out eventStatus);
				MFError.ThrowExceptionForHR(hr);

				Console.WriteLine(string.Format("Media event: " + eventType.ToString()));

				if (Succeeded(eventStatus))
				{
					switch (eventType)
					{
						case MediaEventType.MESessionTopologyStatus:
							int tmpTopoStatus;

							hr = pEvent.GetUINT32(MFAttributesClsid.MF_EVENT_TOPOLOGY_STATUS, out tmpTopoStatus);
							MFError.ThrowExceptionForHR(hr);
							topologyStatus = (MFTopoStatus)tmpTopoStatus;

							switch (topologyStatus)
							{
								case MFTopoStatus.Ready:
									OnTopologyReady(pEvent);
									break;
								case MFTopoStatus.Invalid:
									Console.WriteLine("invalid");
									break;
								case MFTopoStatus.Ended:
									Console.WriteLine("ended");
									break;
								case MFTopoStatus.DynamicChanged:
									Console.WriteLine("changed");
									break;
								case MFTopoStatus.StartedSource:
									Console.WriteLine("started");
									break;
								case MFTopoStatus.SinkSwitched:
									Console.WriteLine("switch");
									break;
								default:
									break;
							}
							break;
						case MediaEventType.MESessionStarted:
							OnSessionStarted(pEvent);
							break;
						case MediaEventType.MESessionPaused:
							OnSessionPaused(pEvent);
							break;
						case MediaEventType.MESessionStopped:
							OnSessionStopped(pEvent);
							break;
						case MediaEventType.MESessionClosed:
							OnSessionClosed(pEvent);
							break;
						case MediaEventType.MESessionEnded:
							OnSessionEnded(pEvent);
							break;
						case MediaEventType.MEEndOfPresentation:
							OnPresentationEnded(pEvent);
							break;
						case MediaEventType.MEBufferingStarted:
							OnBufferingStarted(pEvent);
							break;
						case MediaEventType.MEBufferingStopped:
							OnBufferingStopped(pEvent);
							break;
						case MediaEventType.MESessionCapabilitiesChanged:
							sCaps = (MFSessionCapabilities)MFExtern.MFGetAttributeUINT32(pEvent, MFAttributesClsid.MF_EVENT_SESSIONCAPS, (int)sCaps);
							break;
					}
				}
				else
				{
					SendErrorEvent("Getting status from media event failed with status code: " + eventStatus);
				}
			}
			finally
			{
				// Request another event.
				if (eventType != MediaEventType.MESessionClosed)
				{
					hr = pSession.BeginGetEvent(this, null);
					MFError.ThrowExceptionForHR(hr);
				}

				SafeRelease(pEvent);
			}

			return hr;
		}

	}
}
