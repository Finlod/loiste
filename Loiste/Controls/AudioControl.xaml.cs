﻿using Loiste.src;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Loiste.Controls
{
	public class PrePlayInfo
	{
		public Podcast podcast { get; set; }
		public Episode episode { get; set; }
		public TimeSpan position { get; set; }
		public TimeSpan duration { get; set; }
	}

	public partial class AudioControl : UserControl, IDisposable
	{
		private Thumb VolumeThumb;
		private Thumb AudioProgressThumb;

		private Timer updateVisualsTimer;

		private bool isDragging;
		private double audioProgressMaxValue = 0;
		private bool loaded;

		private PrePlayInfo m_PrePlayInfo;
		public PrePlayInfo PrePlayInfo 
		{
			get
			{
				return m_PrePlayInfo;
			}
			set
			{
				m_PrePlayInfo = value;
				SetPrePlayInformation();
			}
		}

		private const string REWINDSTRING = "\xE100";
		private const string FORWARDSTRING = "\xE101";
		private const string PLAYSTRING = "\xE102";
		private const string PAUSESTRING = "\xE103";
		
		public string CoverSource
		{ 
			get { return (String)GetValue(CoverProperty); }
			set { SetValue(CoverProperty, value); }
		}

		public string TrackText
		{
			get { return (String)GetValue(TrackTextProperty); }
			set { SetValue(TrackTextProperty, value); }
		}

		public Brush SliderColor
		{
			get { return (Brush)GetValue(SliderColorProperty); }
			set { SetValue(SliderColorProperty, value); }
		}

		public static readonly DependencyProperty CoverProperty = DependencyProperty.Register("CoverSource", typeof(string), typeof(AudioControl), new PropertyMetadata("/Resources/nocover.png"));
		public static readonly DependencyProperty TrackTextProperty = DependencyProperty.Register("TrackText", typeof(string), typeof(AudioControl), new PropertyMetadata(""));
		public static readonly DependencyProperty SliderColorProperty = DependencyProperty.Register("SliderColor", typeof(Brush), typeof(AudioControl), new PropertyMetadata());

		public AudioControl()
		{
			InitializeComponent();
			this.DataContext = this;

			if (System.ComponentModel.DesignerProperties.GetIsInDesignMode(this)) return;

			updateVisualsTimer = new Timer();
			updateVisualsTimer.Interval = 500;

			updateVisualsTimer.Elapsed += new ElapsedEventHandler(OnUpdateVisualsTimerElapsed);

			Loaded += new RoutedEventHandler(OnLoaded);
			Unloaded += new RoutedEventHandler(OnUnLoaded);
		}

		~AudioControl()
		{
			Dispose (false);
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		protected virtual void Dispose(bool disposing)
		{
			if (disposing)
			{
				updateVisualsTimer.Dispose();
			}
		}

		private void OnLoaded(object sender, EventArgs e)
		{
			loaded = true;

			App.AudioHandler.ReadyToPlay += new EventHandler(OnReadyToPlay);
			App.AudioHandler.TryingToPlay += new EventHandler(OnTryingToPlay);
			App.AudioHandler.BufferingStarted += new EventHandler(OnBufferingStarted);
			App.AudioHandler.PlaybackStarted += new EventHandler(OnPlaybackStarted);
			App.AudioHandler.PlaybackPaused += new EventHandler(OnPlaybackPaused);
			App.AudioHandler.PlaybackEnded += new EventHandler(OnPlaybackEnded);
			App.AudioHandler.VolumeChanged += new EventHandler(OnVolumeChanged);

			Slider audioProgress = (Slider)AudioSliderArea.FindName("AudioProgress");
			Track progressTrack = (Track)audioProgress.Template.FindName("PART_Track", audioProgress);
			AudioProgressThumb = progressTrack.Thumb;

			Track volumeTrack = (Track)VolumeSlider.Template.FindName("PART_Track", VolumeSlider);
			VolumeThumb = volumeTrack.Thumb;

			VolumeSlider.ValueChanged += new RoutedPropertyChangedEventHandler<double>(VolumeSlider_ValueChanged);
			AudioProgressThumb.MouseEnter += new MouseEventHandler(AudioProgressThumbEnter);

			audioProgressMaxValue = AudioProgress.Maximum;

			SetVolumeValue(App.AudioHandler.GetVolume());

			ResetVisuals(false);

			SetPrePlayInformation();
		}

		private void OnUnLoaded(object sender, RoutedEventArgs e)
		{
			loaded = false;

			App.AudioHandler.ReadyToPlay -= new EventHandler(OnReadyToPlay);
			App.AudioHandler.TryingToPlay -= new EventHandler(OnTryingToPlay);
			App.AudioHandler.BufferingStarted -= new EventHandler(OnBufferingStarted);
			App.AudioHandler.PlaybackStarted -= new EventHandler(OnPlaybackStarted);
			App.AudioHandler.PlaybackPaused -= new EventHandler(OnPlaybackPaused);
			App.AudioHandler.PlaybackEnded -= new EventHandler(OnPlaybackEnded);
			App.AudioHandler.VolumeChanged -= new EventHandler(OnVolumeChanged);

			VolumeSlider.ValueChanged -= new RoutedPropertyChangedEventHandler<double>(VolumeSlider_ValueChanged);
			AudioProgressThumb.MouseEnter -= new MouseEventHandler(AudioProgressThumbEnter);
		}
			

		private void OnPlaybackStarted(object sender, EventArgs e)
		{
			updateVisualsTimer.Start();

			SetPlayIcon(false);
			SetLoadingIcon(false);
		}

		private void OnPlaybackPaused(object sender, EventArgs e)
		{
			updateVisualsTimer.Stop();

			SetPlayIcon();
		}

		private void OnPlaybackEnded(object sender, EventArgs e)
		{
			Console.WriteLine("playback ended");
			ResetVisuals(false);
		}

		private void OnReadyToPlay(object sender, EventArgs e)
		{
			SetPlayIcon(false);
			SetLoadingIcon(false);

			SetDurationText(App.AudioHandler.GetDuration());
		}

		private void OnTryingToPlay(object sender, EventArgs e)
		{
			if (PrePlayInfo == null)
			{
				ResetVisuals();
				SetLoadingIcon();
				SetEpisodeTitleText(App.AudioHandler.CurrentEpisode.Title);
				SetCoverImage(App.AudioHandler.CurrentPodcast.GetPodcastInfo().CoverFilePath);
			}
			else
			{
				SetPrePlayInformation();
			}
		}

		private void OnBufferingStarted(object sender, EventArgs e)
		{
			SetLoadingIcon();
		}

		private void OnVolumeChanged(object sender, EventArgs e)
		{
			SetVolumeValue(App.AudioHandler.GetVolume());
		}

		private void OnUpdateVisualsTimerElapsed(object sender, ElapsedEventArgs e)
		{
			//timer only ticking when audio is playing, or trying to play.

			if (!isDragging)
			{
				if (App.AudioHandler.PlaybackState == src.AudioState.PlaybackState.Buffering) return;

				var position = App.AudioHandler.GetPosition();

				SetCurrentTimeText(position);

				var value = GetSliderValueFromTime(position);

				if (value.HasValue)
					SetAudioProgressValue(value.Value);
			}
		}


		private void PlayPause_Click(object sender, RoutedEventArgs e)
		{
			App.AudioHandler.PlayPause();
		}
		
		private void AudioSliderArea_MouseEnter(object sender, MouseEventArgs e)
		{
			if (AudioProgress.IsEnabled)
				SetThumbOpacity(AudioProgressThumb, 1);
		}

		private void AudioSliderArea_MouseLeave(object sender, MouseEventArgs e)
		{
			if (AudioProgress.IsEnabled)
				SetThumbOpacity(AudioProgressThumb, 0);
		}

		private void VolumeSlider_MouseEnter(object sender, MouseEventArgs e)
		{
			SetThumbOpacity(VolumeThumb, 1);
		}

		private void VolumeSlider_MouseLeave(object sender, MouseEventArgs e)
		{
			SetThumbOpacity(VolumeThumb, 0);
		}



		private void VolumeSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
		{
			if ((float)e.NewValue == App.AudioHandler.GetVolume()) return;

			Console.WriteLine("Volume set " + e.NewValue);
			App.AudioHandler.SetVolume((float)e.NewValue);
		}

		private void AudioProgress_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
		{
			if (isDragging)
			{
				var currentTime = GetTimeFromSliderValue();

				if (!currentTime.HasValue) return;

				SetCurrentTimeText(currentTime.Value);
			}
		}


		private void AudioProgress_DragStarted(object sender, DragStartedEventArgs e)
		{
			isDragging = true;

			//event triggered when clicking and completed fired when mouse up, so lets update currentime
			var currentTime = GetTimeFromSliderValue();
			if (!currentTime.HasValue) return;

			SetCurrentTimeText(currentTime.Value);
		}

		private void AudioProgress_DragCompleted(object sender, DragCompletedEventArgs e)
		{
			isDragging = false;

			var seekTime = GetTimeFromSliderValue();

			Debug.Assert(seekTime.HasValue);

			Task.Factory.StartNew(() =>
			{
				App.AudioHandler.Seek(seekTime.Value);
			});
		}


		private void AudioProgressThumbEnter(object sender, MouseEventArgs e)
		{
			if (e.LeftButton == MouseButtonState.Pressed)
			{
				MouseButtonEventArgs args = new MouseButtonEventArgs(e.MouseDevice, e.Timestamp, MouseButton.Left);
				args.RoutedEvent = MouseLeftButtonDownEvent;
				AudioProgressThumb.RaiseEvent(args);
			}
		}


		private void ResetVisuals(bool enabled = true)
		{
			Action reset = () =>
			{
				SetThumbOpacity(AudioProgressThumb, 0);
				SetThumbOpacity(VolumeThumb, 0);

				AudioProgress.Value = 0;

				SetPlayIcon();
				SetLoadingIcon(false);

				SetCurrentTimeText(TimeSpan.FromSeconds(0));
				SetDurationText(TimeSpan.FromSeconds(0));
				SetEpisodeTitleText("No episode to play");
				SetCoverImage("podcats/nocover.png");

				if (enabled)
				{
					AudioProgress.IsEnabled = true;
					RewindButton.IsEnabled = true;
					ForwardButton.IsEnabled = true;
				}
				else
				{
					AudioProgress.IsEnabled = false;
					RewindButton.IsEnabled = false;
					ForwardButton.IsEnabled = false;
				}
			};

			if (CheckAccess())
			{
				reset();
			}
			else
			{
				Dispatcher.BeginInvoke(reset);
			}

			updateVisualsTimer.Stop();
		}


		private void SetPlayIcon(bool play = true)
		{
			Action setIcon = () =>
			{
				//TextBlock textblock = (TextBlock)PlayPauseButton.Template.FindName("PlayPauseText", PlayPauseButton);
				PlayPauseButton.Content = (play == true) ? PLAYSTRING : PAUSESTRING;
			};

			if (CheckAccess())
			{
				setIcon();
			}
			else
			{
				Dispatcher.BeginInvoke(setIcon);
			}
		}

		private void SetLoadingIcon(bool isLoading = true)
		{
			Action setIcon = () =>
			{
				if (isLoading)
				{
					LoadingRing.Visibility = System.Windows.Visibility.Visible;
					PlayPauseButton.Visibility = System.Windows.Visibility.Collapsed;
					LoadingRing.IsActive = true;
				}
				else
				{
					LoadingRing.IsActive = false;
					LoadingRing.Visibility = System.Windows.Visibility.Collapsed;
					PlayPauseButton.Visibility = System.Windows.Visibility.Visible;
				}
			};

			if (CheckAccess())
			{
				setIcon();
			}
			else
			{
				Dispatcher.BeginInvoke(setIcon);
			}
		}

		private void SetThumbOpacity(Thumb thumb, double toOpacity)
		{
			Duration d = new Duration(new TimeSpan(0, 0, 0, 0, 150));
			DoubleAnimation x = new DoubleAnimation(thumb.Opacity, toOpacity, d);

			Storyboard.SetTarget(x, thumb);
			Storyboard.SetTargetProperty(x, new PropertyPath("Opacity"));

			Storyboard sb = new Storyboard();
			sb.Children.Add(x);
			sb.Begin();
		}


		private void SetAudioProgressValue(double value)
		{
			Action setValue = () =>
			{
				AudioProgress.Value = value;
			};

			if (CheckAccess())
			{
				setValue();
			}
			else
			{
				Dispatcher.BeginInvoke(setValue);
			}
		}

		private void SetCurrentTimeText(TimeSpan currentTime)
		{
			Action setText = () =>
			{
				if (currentTime < TimeSpan.FromMinutes(60))
				{
					AudioCurrentTime.Content = currentTime.ToString(@"mm\:ss");
				}
				else
				{
					AudioCurrentTime.Content = currentTime.ToString(@"hh\:mm\:ss");
				}
			};

			if (CheckAccess())
			{
				setText();
			}
			else
			{
				Dispatcher.BeginInvoke(setText);
			}
		}

		private void SetDurationText(TimeSpan duration)
		{
			Action setText = () => {
				if (duration < TimeSpan.FromMinutes(60))
				{
					AudioTotalTime.Content = duration.ToString(@"mm\:ss");
				}
				else
				{
					AudioTotalTime.Content = duration.ToString(@"hh\:mm\:ss");
				}
			};

			if (CheckAccess())
			{
				setText();
			}
			else
			{
				Dispatcher.BeginInvoke(setText);
			}
		}

		private void SetEpisodeTitleText(string title)
		{
			Action setText = () =>
			{
				EpisodeTitle.Text = title;
			};

			if (CheckAccess())
			{
				setText();
			}
			else
			{
				Dispatcher.BeginInvoke(setText);
			}
		}

		private void SetCoverImage(string coverPath)
		{
			Action setCover = () =>
			{
				PodcastCover.Source = Utils.SafeBitmapGet(coverPath, new Point(PodcastCover.Width, PodcastCover.Height));
			};

			if (CheckAccess())
			{
				setCover();
			}
			else
			{
				Dispatcher.BeginInvoke(setCover);
			}
		}

		private void SetVolumeValue(double value)
		{
			Action setValue = () =>
			{
				VolumeSlider.Value = value;
				Console.WriteLine("volumeslider value set " + value);
			};

			if (CheckAccess())
			{
				setValue();
			}
			else
			{
				Dispatcher.BeginInvoke(setValue);
			}
		}


		private double? GetSliderValueFromTime(TimeSpan time, TimeSpan? _duration = null)
		{
			double durationInSeconds;

			if (_duration.HasValue)
			{
				durationInSeconds = _duration.Value.TotalSeconds;
			}
			else
			{
				durationInSeconds = App.AudioHandler.GetDuration().TotalSeconds;
			}

			if (durationInSeconds == 0) return null;

			var maxValue = audioProgressMaxValue;

			double value = (time.TotalSeconds / durationInSeconds) * maxValue;

			return value;

		}

		private TimeSpan? GetTimeFromSliderValue()
		{
			Func<TimeSpan?> getTime = () =>
			{
				var duration = App.AudioHandler.GetDuration().TotalSeconds;

				if (duration == 0) return null;

				var maxValue = audioProgressMaxValue;
				var value = AudioProgress.Value;

				double timeInSeconds = (value / maxValue) * duration;

				return TimeSpan.FromSeconds(timeInSeconds);
			};

			if (CheckAccess())
			{
				return getTime();
			}
			else
			{
				return (TimeSpan?)Dispatcher.BeginInvoke(getTime).Result;
			}
			
		}


		private void SetPrePlayInformation()
		{
			if (!loaded) return;

			if (PrePlayInfo == null) return;

			ResetVisuals(true);

			SetLoadingIcon(true);

			SetCurrentTimeText(PrePlayInfo.position);
			SetDurationText(PrePlayInfo.duration);

			SetCoverImage(PrePlayInfo.podcast.GetPodcastInfo().CoverFilePath);
			SetEpisodeTitleText(PrePlayInfo.episode.Title);

			var sliderValue = GetSliderValueFromTime(PrePlayInfo.position, PrePlayInfo.duration);
			if (sliderValue.HasValue)
			{
				Console.WriteLine("trying to set value :" + sliderValue.Value);
				SetAudioProgressValue(sliderValue.Value);
			}

			PrePlayInfo = null;
		}
	}
}
