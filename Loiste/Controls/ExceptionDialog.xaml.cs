﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Loiste.Controls
{
	public partial class ExceptionDialog : Dialog
	{
		public ExceptionDialog(Grid parent, string message) : base(parent)
		{
			InitializeComponent();
			this.DataContext = this;

			StaysOpen = true;

			MessageText.Text = message;
		}

		private void OkButton_Click(object sender, RoutedEventArgs e)
		{
			IsOpen = false;
		}
	}
}
