﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Loiste.Controls
{
	public partial class AddPodcastDialog : Dialog
	{
		public AddPodcastDialog(Grid parent) : base(parent)
		{
			InitializeComponent();
			this.DataContext = this;
		}

		private async void AddPodcast_Click(object sender, RoutedEventArgs e)
		{
			IsOpen = false;
			try
			{
				await App.PodcastHandler.AddPodcastAsync(RssUrlBox.Text).ConfigureAwait(false);
			}
			catch (RefreshFailedException ex)
			{
				if (ex.m_Status == RefreshStatus.NETWORKERROR)
				{
					Action ExDialog = () =>
					{
						new ExceptionDialog(parent, ex.Message);
					};

					await Dispatcher.BeginInvoke(ExDialog);

				}
			}
			
		}

		private void CloseDialog_Click(object sender, RoutedEventArgs e)
		{
			IsOpen = false;
		}
	}
}
