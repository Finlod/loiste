﻿using System;
using System.Windows.Shapes;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Loiste.src;

namespace Loiste.Controls
{
	public partial class EditPodcastInfoDialog : Dialog
	{
		public Guid m_PodcastGuid;

		private Rectangle m_Rect = new Rectangle();
		private PodcastInfo m_TmpInfo = new PodcastInfo();
		private Podcast m_Podcast;

		private bool m_Loaded;

		public EditPodcastInfoDialog(Grid parent) : base(parent)
		{
			InitializeComponent();
			this.DataContext = this;

			Loaded += new RoutedEventHandler(OnLoaded);
			Unloaded += new RoutedEventHandler(OnUnLoaded);
		}

		private void OnUnLoaded(object sender, RoutedEventArgs e)
		{
			m_Loaded = false;

			parent.Children.Remove(m_Rect);
		}

		private void OnLoaded(object sender, RoutedEventArgs e)
		{
			m_Loaded = true;

			m_Podcast = App.PodcastHandler.GetPodcastByGuid(m_PodcastGuid);
			//TODO: fix url width going too big.

			RenderOptions.SetBitmapScalingMode(PodcastCover, BitmapScalingMode.Fant); //TODO: fix this hack
			PodcastCover.Source = Utils.SafeBitmapGet(m_Podcast.GetPodcastInfo().CoverFilePath);
			PodcastName.Text = m_Podcast.GetPodcastInfo().Title;
			PodcastFeedUrl.Text = m_Podcast.GetPodcastInfo().FeedUrl;

			if (m_Podcast.m_HaveCustominfo)
			{
				ResetButton.IsEnabled = true;
			}


			//darkening effect
			m_Rect.HorizontalAlignment = HorizontalAlignment.Stretch;
			m_Rect.VerticalAlignment = VerticalAlignment.Stretch;
			m_Rect.Fill = new SolidColorBrush(Color.FromArgb(150, 0, 0, 0));

			parent.Children.Insert(parent.Children.Count - 1, m_Rect);
		}

		private void ResetButton_Click(object sender, RoutedEventArgs e)
		{
			PodcastFeedUrl.Text = m_Podcast.m_DefaultPodcastInfo.FeedUrl;
			PodcastName.Text = m_Podcast.m_DefaultPodcastInfo.Title;

			ResetButton.IsEnabled = false;
		}

		private void SaveButton_Click(object sender, RoutedEventArgs e)
		{
			//m_Podcast.m_PodcastInfo.CoverFilePath = PodcastFeedUrl.Text;
			m_Podcast.m_CustomPodcastInfo.Title = PodcastName.Text == m_Podcast.m_DefaultPodcastInfo.Title ? "" : PodcastName.Text;
			m_Podcast.m_CustomPodcastInfo.FeedUrl = PodcastFeedUrl.Text == m_Podcast.m_DefaultPodcastInfo.FeedUrl ? "" : PodcastFeedUrl.Text;

			App.PodcastHandler.SavePodcastsToFile();

			IsOpen = false;
		}

		private void CancelButton_Click(object sender, RoutedEventArgs e)
		{
			IsOpen = false;
		}

		private void ChooseFileButton_Click(object sender, RoutedEventArgs e)
		{

		}

		private void PodcastName_TextChanged(object sender, TextChangedEventArgs e)
		{
			if (m_Loaded && PodcastName.Text != m_Podcast.m_DefaultPodcastInfo.Title)
			{
				ResetButton.IsEnabled = true;
			}

			m_TmpInfo.Title = PodcastName.Text;
		}

		private void PodcastFeedUrl_TextChanged(object sender, TextChangedEventArgs e)
		{
			if (m_Loaded && PodcastFeedUrl.Text != m_Podcast?.m_DefaultPodcastInfo.FeedUrl)
			{
				ResetButton.IsEnabled = true;
			}

			m_TmpInfo.FeedUrl = PodcastFeedUrl.Text;
		}
	}
}
