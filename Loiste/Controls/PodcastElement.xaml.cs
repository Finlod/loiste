﻿using Loiste.Pages;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Loiste.src;

namespace Loiste.Controls
{
	public partial class PodcastElement : UserControl
	{
		public ImageSource CoverSource
		{
			get { return (ImageSource)GetValue(CoverSourceProperty); }
			set { SetValue(CoverSourceProperty, value); }
		}

		public string PodcastName
		{
			get { return (String)GetValue(PodcastNameProperty); }
			set { SetValue(PodcastNameProperty, value); }
		}

		public static readonly DependencyProperty CoverSourceProperty = DependencyProperty.Register("CoverSource", typeof(ImageSource), typeof(PodcastElement), new PropertyMetadata(new BitmapImage(new Uri("pack://application:,,,/Resources/nocover.png"))));
		public static readonly DependencyProperty PodcastNameProperty = DependencyProperty.Register("PodcastName", typeof(string), typeof(PodcastElement), new PropertyMetadata("Podcast name"));

		private const string PLAYSTRING = "\xE102";
		private const string PAUSESTRING = "\xE103";

		public Podcast podcast;
		private NavigationService navigationService;

		private PodcastElement()
		{
			InitializeComponent();
			this.DataContext = this;

			Loaded += new RoutedEventHandler(OnLoaded);
			Unloaded += new RoutedEventHandler(OnUnLoaded);
		}

		public PodcastElement(Podcast podcast, NavigationService naviService) : this()
		{
			this.podcast = podcast;
			this.navigationService = naviService;
		}

		private void OnLoaded(object sender, RoutedEventArgs e)
		{
			//App.AudioHandler.TryingToPlay += new EventHandler(OnPlaybackChanged);
			App.AudioHandler.PlaybackStarted += new EventHandler(OnPlaybackChanged);
			App.AudioHandler.PlaybackPaused += new EventHandler(OnPlaybackChanged);
			App.AudioHandler.PlaybackEnded += new EventHandler(OnPlaybackChanged);
			App.AudioHandler.PlaybackStopped += new EventHandler(OnPlaybackChanged);

			podcast.PodcastRefreshed += new EventHandler(OnPodcastRefreshed);

			PodcastName = podcast.GetPodcastInfo().Title;

			CoverSource = Utils.SafeBitmapGet(podcast.GetPodcastInfo().CoverFilePath, new Point(Cover.Height, Cover.Width));

			EditPodcast.Tag = podcast.m_Guid;

			UpdatePlayIcon();
		}

		private void OnUnLoaded(object sender, RoutedEventArgs e)
		{
			//App.AudioHandler.TryingToPlay -= new EventHandler(OnPlaybackChanged);
			App.AudioHandler.PlaybackStarted -= new EventHandler(OnPlaybackChanged);
			App.AudioHandler.PlaybackPaused -= new EventHandler(OnPlaybackChanged);
			App.AudioHandler.PlaybackEnded -= new EventHandler(OnPlaybackChanged);
			App.AudioHandler.PlaybackStopped -= new EventHandler(OnPlaybackChanged);

			podcast.PodcastRefreshed -= new EventHandler(OnPodcastRefreshed);
		}


		private void OnPlaybackChanged(object sender, EventArgs e)
		{
			if (App.AudioHandler.CurrentPodcast == podcast)
			{
				UpdatePlayIcon();
			}
			else
			{
				SetPlayIcon(false);
			}
		}

		private void OnPodcastRefreshed(object sender, EventArgs e)
		{
			this.podcast = (Podcast)sender;

			Action updateInfo = () =>
			{
				CoverSource = Utils.SafeBitmapGet(podcast.GetPodcastInfo().CoverFilePath, new Point(Cover.Height, Cover.Width));
				PodcastTitle.Text = podcast.GetPodcastInfo().Title;
            };

			if (CheckAccess())
			{
				updateInfo();
			}
			else
			{
				Dispatcher.BeginInvoke(updateInfo);
			}
		}


		private void PlayPauseButton_Click(object sender, RoutedEventArgs e)
		{
			e.Handled = true;

			if (App.AudioHandler.CurrentPodcast == podcast)
			{
				App.AudioHandler.PlayPause();
			}
			else
			{
				Task.Run(() => App.AudioHandler.Play(podcast, podcast.m_Episodes[0]));

			}
		}

		private void HoverContent_Click(object sender, RoutedEventArgs e)
		{
			e.Handled = true;

			ShowPodcastInfoPage();
		}

		private void RemovePodcast_Click(object sender, RoutedEventArgs e)
		{
			e.Handled = true;
			App.PodcastHandler.RemovePodcast(podcast.m_Guid);
		}

		private void EditPodcast_Click(object sender, RoutedEventArgs e)
		{
			e.Handled = false;
		}

		private void PodcastName_Click(object sender, RoutedEventArgs e)
		{
			e.Handled = true;

			ShowPodcastInfoPage();

		}


		private void UpdatePlayIcon()
		{
			Action updateIcon = () =>
			{
				if (App.AudioHandler.CurrentPodcast != podcast)
				{
					PlayPauseButton.Content = PLAYSTRING;
				} else
				{
					if (App.AudioHandler.PlaybackState == src.AudioState.PlaybackState.Playing)
					{
						PlayPauseButton.Content = PAUSESTRING;
					}
					else
					{
						PlayPauseButton.Content = PLAYSTRING;
					}
				}
			};

			if (CheckAccess())
			{
				updateIcon();
			}
			else
			{
				Dispatcher.BeginInvoke(updateIcon);
			}

		}

		private void SetPlayIcon(bool playing = true)
		{
			Action setIcon = () =>
			{
				if (playing)
				{
					PlayPauseButton.Content = PAUSESTRING;
				}
				else
				{
					PlayPauseButton.Content = PLAYSTRING;
				}
			};

			if (CheckAccess())
			{
				setIcon();
			}
			else
			{
				Dispatcher.BeginInvoke(setIcon);
			}
		}

		private void ShowPodcastInfoPage()
		{
			Debug.Assert(podcast != null);
			Debug.Assert(navigationService != null);

			navigationService.Navigate(App.PodcastInfoPage);
			App.PodcastInfoPage.SetInformation(podcast);
		}
	}
}
