﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Loiste.Controls
{
	//A dialog which when created is automaticly removed (when set IsOpen false).
	public partial class Dialog : ContentControl
	{
		private Grid m_LayoutRoot;
		private bool m_IsOpen = true;
		protected Grid parent;

		public bool IsOpen
		{
			get
			{
				return m_IsOpen;
			}
			set
			{
				if (m_LayoutRoot != null)
				{
					if (!value)
					{
						//m_LayoutRoot.Visibility = System.Windows.Visibility.Collapsed;
						if (Parent is Panel)
						{
							(Parent as Panel).Children.Remove(this);
						}
						else
						{
							throw new InvalidCastException("Dialog control cannot have parent wich doensn't inherit from Panel");
						}
					}
				}

				m_IsOpen = value;
			}
		}

		public bool StaysOpen { get; set; }

		static Dialog()
		{
			DefaultStyleKeyProperty.OverrideMetadata(typeof(Dialog), new FrameworkPropertyMetadata(typeof(Dialog)));
		}

		public Dialog(Grid parent)
		{
			this.parent = parent;
			parent.Children.Add(this);
		}

		public override void OnApplyTemplate()
		{
			base.ApplyTemplate();

			m_LayoutRoot = (Grid)Template.FindName("LayoutRoot", this);
			m_LayoutRoot.MouseDown += new MouseButtonEventHandler(OnMouseDownLayoutRoot);

			IsOpen = IsOpen;
		}

		private void OnMouseDownLayoutRoot(object sender, MouseButtonEventArgs e)
		{
			e.Handled = true;

			if (StaysOpen) return;

			if (e.OriginalSource != m_LayoutRoot) return;

			IsOpen = false;
		}

	}
}
