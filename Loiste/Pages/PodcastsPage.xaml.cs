﻿using Loiste.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.Specialized;

namespace Loiste.Pages
{
	public partial class PodcastsPage : Page
	{
		private bool added;


		public PodcastsPage()
		{
			InitializeComponent();
			this.DataContext = this;

			App.PodcastHandler.PodcastListChanged += new NotifyCollectionChangedEventHandler(OnPodcastListChanged);
			
			Loaded += new RoutedEventHandler(OnLoaded);
			Unloaded += new RoutedEventHandler(OnUnloaded);
		}

		private void OnLoaded(object sender, RoutedEventArgs e)
		{
			AddElements();
		}

		private void OnUnloaded(object sender, RoutedEventArgs e)
		{
			RemoveElements();
		}


		private void OnPodcastListChanged(object sender, NotifyCollectionChangedEventArgs e)
		{
			if (e.Action == NotifyCollectionChangedAction.Add)
			{
				foreach (Podcast item in e.NewItems)
				{
					AddPodcastElement(item.m_Guid);
				}
				
			}
			else if (e.Action == NotifyCollectionChangedAction.Remove)
			{
				foreach (Podcast item in e.OldItems)
				{
					RemovePodcastElement(item.m_Guid);
				}
			}
			else
			{
				Console.WriteLine("Unknown podcast list change: " + e.Action);
			}
		}

		private void AddElements()
		{
			if (added) return;


			for (int i = 0; i < App.PodcastHandler.m_Podcasts.Count(); ++i)
			{
				var podcast = App.PodcastHandler.m_Podcasts[i];

				var podcastElement = new PodcastElement(podcast, NavigationService);
				podcastElement.EditPodcast.Click += new RoutedEventHandler(OnEditPodcastClick);
				PodcastElements.Children.Insert(i, podcastElement);
			}

			added = true;
		}

		private void RemoveElements()
		{
		}

		private void AddPodcastElement(Guid podcastGuid)
		{
			Action addElement = () =>
			{
				var podcast = App.PodcastHandler.GetPodcastByGuid(podcastGuid);
				var podcastElement = new PodcastElement(podcast, NavigationService);
				podcastElement.EditPodcast.Click += new RoutedEventHandler(OnEditPodcastClick);
				PodcastElements.Children.Insert(PodcastElements.Children.Count - 1, podcastElement);
			};

			if (CheckAccess())
			{
				addElement();
			}
			else
			{
				Dispatcher.BeginInvoke(addElement);
			}
		}

		private void OnEditPodcastClick(object sender, RoutedEventArgs e)
		{
			e.Handled = true;

			new EditPodcastInfoDialog(LayoutRoot).m_PodcastGuid = (Guid)(sender as Button).Tag;
		}

		private void RemovePodcastElement(Guid podcastGuid)
		{
			Action<UIElement> removeElement = (item) =>
			{
				PodcastElements.Children.Remove(item);
			};

			
			for (int i = PodcastElements.Children.Count - 1; i >= 0; i--)
			{
				var item = PodcastElements.Children[i];

				if (!(item is PodcastElement)) continue;
 
				PodcastElement podcastElement = (PodcastElement)item;

				if (podcastElement.podcast.m_Guid == podcastGuid)
				{
					if (CheckAccess())
					{
						removeElement(item);
					}
					else
					{
						Dispatcher.BeginInvoke(removeElement, item);
					}
					
					return;
				}
			}
		}

		private void AddPodcast_Click(object sender, EventArgs e)
		{
			new AddPodcastDialog(LayoutRoot);
		}
	}
}
