﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Loiste.Pages
{
	/// <summary>
	/// Interaction logic for DebugPage.xaml
	/// </summary>
	public partial class DebugPage : Page
	{
		public DebugPage()
		{
			InitializeComponent();
			listBox.ItemsSource = App.NotificationCenter.m_Notifications;
		}

		private void savepodcaststofile_Click(object sender, RoutedEventArgs e)
		{
			MethodInfo saveMethod = App.PodcastHandler.GetType().GetMethod("SavePodcastsToFile", BindingFlags.NonPublic | BindingFlags.Instance);
			saveMethod.Invoke(App.PodcastHandler, null);
		}

		private void loadpodcastfromfile_Click(object sender, RoutedEventArgs e)
		{
			MethodInfo loadMethod = App.PodcastHandler.GetType().GetMethod("LoadPodcasts", BindingFlags.NonPublic | BindingFlags.Instance);
			loadMethod.Invoke(App.PodcastHandler, null);
		}

		private void button_Click(object sender, RoutedEventArgs e)
		{
			Action setCover = () =>
			{
				listBox.ItemsSource = App.NotificationCenter.m_Notifications.ToArray();
			};

			if (CheckAccess())
			{
				setCover();
			}
			else
			{
				Dispatcher.BeginInvoke(setCover);
			}
			
		}
	}
}
