﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Loiste.src;
using System.ComponentModel;


namespace Loiste.Pages
{
	public class PlayInfoItem : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		public Guid episodeGuid;
		public string Title { get; set; }
		public DateTime ReleaseDate { get; set; }

		AudioState.PlaybackState m_PlaybackState;

		public AudioState.PlaybackState PlaybackState
		{
			get
			{
				return m_PlaybackState;
			}
			set
			{
				if (m_PlaybackState != value)
				{
					m_PlaybackState = value;
					OnPropertyChanged("PlaybackState");
				}
			}
		}

		void OnPropertyChanged(string propertyName)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}

	}

	public partial class PodcastInfoPage : Page
	{
		private Podcast podcast;
		private PlayInfoItem playingItem;
		private List<PlayInfoItem> items;

		public PodcastInfoPage()
		{
			InitializeComponent();
			this.DataContext = this;

			items = new List<PlayInfoItem>();

			EpisodeList.SelectionChanged += new SelectionChangedEventHandler(OnSelectionChanged);

			Loaded += new RoutedEventHandler(OnLoaded);
			Unloaded += new RoutedEventHandler(OnUnLoaded);

			Focus();
		}

		private void OnLoaded(object sender, RoutedEventArgs e)
		{
			App.AudioHandler.TryingToPlay += new EventHandler(OnPlaybackChanged);
			App.AudioHandler.PlaybackStarted += new EventHandler(OnPlaybackChanged);
			App.AudioHandler.PlaybackPaused += new EventHandler(OnPlaybackChanged);
			App.AudioHandler.PlaybackEnded += new EventHandler(OnPlaybackChanged);
			App.AudioHandler.PlaybackStopped += new EventHandler(OnPlaybackChanged);

			if (EpisodeList.Items.Count > 0)
				EpisodeList.ScrollIntoView(EpisodeList.Items[0]);

			Console.WriteLine("info loaded");
		}

		private void OnUnLoaded(object sender, RoutedEventArgs e)
		{
			App.AudioHandler.TryingToPlay -= new EventHandler(OnPlaybackChanged);
			App.AudioHandler.PlaybackStarted -= new EventHandler(OnPlaybackChanged);
			App.AudioHandler.PlaybackPaused -= new EventHandler(OnPlaybackChanged);
			App.AudioHandler.PlaybackEnded -= new EventHandler(OnPlaybackChanged);
			App.AudioHandler.PlaybackStopped -= new EventHandler(OnPlaybackChanged);

			Console.WriteLine("info unloaded");

			EpisodeList.ItemsSource = null;
			items.Clear();

			podcast = null;
			playingItem = null;
		}

		public void SetInformation(Podcast podcast)
		{
			this.podcast = podcast;

			SetPodcastInfo();
			SetEpisodeList();
			UpdatePlayingItemInfo();
		}


		private void OnSelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (e.AddedItems.Count == 0) return;

			var episode = podcast.GetEpisodeByGuid((e.AddedItems[0] as PlayInfoItem).episodeGuid);
			SetEpisodeInfo(episode);
		}

		private void OnPlaybackChanged(object sender, EventArgs e)
		{
			if (podcast == null) return;

			UpdatePlayingItemInfo();
		}


		private void EpisodePlayPause_Click(object sender, RoutedEventArgs e)
		{
			e.Handled = true;

			PlayInfoItem ep = (PlayInfoItem)(e.OriginalSource as Button).DataContext;

			var episode = podcast.GetEpisodeByGuid(ep.episodeGuid);

			if (App.AudioHandler.CurrentEpisode == episode)
			{
				App.AudioHandler.PlayPause();
			}
			else
			{
				Task.Run(() => App.AudioHandler.Play(podcast, episode));
			}
		}

		private void BackButton_Click(object sender, RoutedEventArgs e)
		{
			NavigationService.GoBack();
		}


		private void UpdatePlayingItemInfo()
		{
			if (App.AudioHandler.CurrentEpisode == null) return;

			if (playingItem == null)
			{
				SetNewPlayingItem();
			}
			else
			{
				if (playingItem.episodeGuid == App.AudioHandler.CurrentEpisode.m_Guid)
				{
					playingItem.PlaybackState = App.AudioHandler.PlaybackState;
				}
				else
				{
					//episode changed, need to set old item playbackstate back to stopped and then set new one
					playingItem.PlaybackState = AudioState.PlaybackState.Stopped;

					//finds and sets playbackstate for new item
					SetNewPlayingItem();
				}
			}
		}

		private void SetNewPlayingItem()
		{
			var item = EpisodeList.Items.Cast<PlayInfoItem>().Where(x => x.episodeGuid == App.AudioHandler.CurrentEpisode.m_Guid).FirstOrDefault();
			if (item == null) return;

			playingItem = item;
			playingItem.PlaybackState = App.AudioHandler.PlaybackState;
		}

		
		private void SetEpisodeList()
		{
			items.Capacity = podcast.m_Episodes.Count() + 1;

			foreach (var episode in podcast.m_Episodes)
			{
				var item = new PlayInfoItem
				{
					episodeGuid = episode.m_Guid,
					Title = episode.Title,
					ReleaseDate = episode.RelaseDate,
				};

				items.Add(item);
			}

			EpisodeList.ItemsSource = items;

			EpisodeList.SelectedIndex = 0;
		}

		private void SetPodcastInfo()
		{
			Action setInfo = () =>
			{
				RenderOptions.SetBitmapScalingMode(this, BitmapScalingMode.Fant); //TODO: fix this hack
				PodcastCover.Source = Utils.SafeBitmapGet(podcast.GetPodcastInfo().CoverFilePath, new Point(PodcastCover.MaxWidth, PodcastCover.MaxHeight));

				PodcastTitle.Text = podcast.GetPodcastInfo().Title;
				PodcastDescription.Text = podcast.GetPodcastInfo().Description;
				PodcastEpisodeCount.Text = podcast.m_Episodes.Count() + " episodes";
			};

			if (CheckAccess())
			{
				setInfo();
			}
			else
			{
				Dispatcher.BeginInvoke(setInfo);
			}
		}

		private void SetEpisodeInfo(Episode episode)
		{
			Action setInfo = () =>
			{
				EpisodeTitle.Text = episode.Title;
				EpisodeDescription.Text = episode.Description;

				if (episode.Duration != TimeSpan.Zero)
				{
					EpisodeDuration.Text = Utils.ToShortForm(episode.Duration);
					EpisodeDuration.Visibility = System.Windows.Visibility.Visible;
				}
			};

			if (CheckAccess())
			{
				setInfo();
			}
			else
			{
				Dispatcher.BeginInvoke(setInfo);
			}
		}

		private void EpisodeList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			var dataContext = ((FrameworkElement)e.OriginalSource).DataContext;

			if (dataContext == null) return;

			if (!(dataContext is PlayInfoItem)) return;

			var ep = dataContext as PlayInfoItem;

			var episode = podcast.GetEpisodeByGuid(ep.episodeGuid);

			if (App.AudioHandler.CurrentEpisode == episode)
			{
				App.AudioHandler.PlayPause();
			}
			else
			{
				Task.Run(() => App.AudioHandler.Play(podcast, episode));
			}
		}
	}
}
