﻿using Loiste.Pages;
using Loiste.src;
using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace Loiste
{
	public class SessionInfo
	{
		public Guid podcastGuid;
		public Guid episodeGuid;
		public TimeSpan position;
		public TimeSpan duration;
		public float volume;
	}

	public partial class App : Application
	{
		private string m_SettingsFilename = "settings.json";
		private string m_SessionFilename = "session.json";

		public static Settings Settings = new Settings();
		public static readonly NotificationCenter NotificationCenter = new NotificationCenter();
		public static readonly AudioHandler AudioHandler = new AudioHandler();
		public static readonly PodcastHandler PodcastHandler = new PodcastHandler();

		//Pages
		public static PodcastInfoPage PodcastInfoPage;
		public static PodcastsPage PodcastsPage;
		public static SettingsPage SettingsPage;

		private void Application_Startup(object sender, StartupEventArgs e)
		{
			DispatcherUnhandledException += new System.Windows.Threading.DispatcherUnhandledExceptionEventHandler(OnUnhandledException);

			if (System.Diagnostics.Debugger.IsAttached)
			{
				Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-us");
			}

			LoadSettings();

			if (Settings.m_LoadMFOnStartup)
			{
				AudioHandler.Initialize();
			}

			if (Settings.m_RefreshOnStartup)
			{
				Task.Run((Func<Task>)PodcastHandler.RefreshPodcastsAsync).ConfigureAwait(false);
			}

			PodcastInfoPage = new PodcastInfoPage();
			PodcastsPage = new PodcastsPage();
			SettingsPage = new SettingsPage();
		}

		private void OnUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
		{
			Exception ex = e.Exception;
			MessageBox.Show(ex.Message + " --- stack trace: " + ex.ToString(), "Uncaught Thread Exception",
							MessageBoxButton.OK, MessageBoxImage.Error);

			Shutdown(-1);
		}

		private void Application_Exit(object sender, ExitEventArgs e)
		{
			SaveSettings();
			App.AudioHandler.Stop();
			SaveSessionInfo();
			App.AudioHandler.Shutdown();
		}

		private void LoadSettings()
		{
			if (!File.Exists(m_SettingsFilename)) return;

			string settingsFile = "";

			Utils.SafeFileRead(new Uri(m_SettingsFilename, UriKind.Relative), () =>
			{
				settingsFile = System.IO.File.ReadAllText(m_SettingsFilename);
			});
			
			Settings = JsonConvert.DeserializeObject<Settings>(settingsFile);
		}


		public void SaveSettings()
		{
			string serializedSettings = JsonConvert.SerializeObject(Settings, Newtonsoft.Json.Formatting.Indented);

			Utils.SafeFileWrite(new Uri(m_SettingsFilename, UriKind.Relative), () =>
			{
				System.IO.File.WriteAllText(m_SettingsFilename, serializedSettings);
			});
		}

		public void SaveSessionInfo()
		{
			var podcast = AudioHandler.CurrentPodcast;
			var episode = AudioHandler.CurrentEpisode;

			SessionInfo sessionInfo = new SessionInfo();

			if (episode != null && podcast != null)
			{
				sessionInfo.podcastGuid = podcast.m_Guid;
				sessionInfo.episodeGuid = episode.m_Guid;
				sessionInfo.position = AudioHandler.GetPosition();
				sessionInfo.duration = AudioHandler.GetDuration();
				sessionInfo.volume = AudioHandler.GetVolume();
			}
			else
			{
				sessionInfo.podcastGuid = Guid.Empty;
				sessionInfo.episodeGuid = Guid.Empty;
				sessionInfo.position = TimeSpan.Zero;
				sessionInfo.duration = TimeSpan.Zero;
				sessionInfo.volume = AudioHandler.GetVolume();
			}

			string serializedInfo = JsonConvert.SerializeObject(sessionInfo, Newtonsoft.Json.Formatting.Indented);

			Utils.SafeFileWrite(new Uri(m_SessionFilename, UriKind.Relative), () =>
			{
				System.IO.File.WriteAllText(m_SessionFilename, serializedInfo);
			});
			
		}

		public SessionInfo LoadSessionInfo()
		{
			SessionInfo lastSessionInfo;

			if (!File.Exists(m_SessionFilename)) return new SessionInfo();

			string lastSessionJsonFile = "";

			Utils.SafeFileRead(new Uri(m_SessionFilename, UriKind.Relative), () =>
			{
				lastSessionJsonFile = System.IO.File.ReadAllText(m_SessionFilename);
			});

			lastSessionInfo = JsonConvert.DeserializeObject<SessionInfo>(lastSessionJsonFile);

			return lastSessionInfo;
		}
	}
}
